
LOADING SCREEN PRO
=============================

An editor script by Martin Nerurkar 

- Visit my site at www.playful.systems
- Visit my company at www.sharkbombs.com
- Asset Store Page: https://www.assetstore.unity3d.com/en/#!/content/85515


Description
-----------------------------
A lighting fast option to add your own loading screen! Use one of the included loading screens to get started right away or create your own.

Also includes the HEALTH & PROGRESS BARS PRO asset for smooth animations.

See more: https://www.assetstore.unity3d.com/#!/content/82405

Interactive Demo: http://www.playful.systems/demos/loadingscreenpro/

Features:

- One line integration into your project
- 8 pre-designed loading scenes and more to come...
- Show preview info about the scene being loaded
- Can display random game tips
- Easily extendable

The example loading scenes are built using the Unity UI but you can create 3d loading scenes if you want with no difficulty. 
All the source code is included so you can extend the assets as much as you want!


Contact & Support
-----------------------------
If you have any questions about the asset, please don't hesitate to
reach out to me:

	E-Mail: 	support@playful.systems
	Twitter: 	@mnerurkar

And if you find the time, I'd appreciate a review on the asset store.


