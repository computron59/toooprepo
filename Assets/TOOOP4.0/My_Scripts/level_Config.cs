﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;



// so scripts should link to this and get the config. The levelselect and hudscript should call this
public class level_Config : MonoBehaviour {


    public string[] nonGravityScenes;
    public string[] withGravityScenes;

    public AItype setAIConfig;
    public bool isAIlevel = false;
    public bool gravity = false;    // the ball already checks this
    //public bool simplePopMode = false;
    //public bool laserMode = true;
    //public bool disruptorMethod = false;
    public bool autoLasers = true;
    public string nextLevelToLoad;
    public string[] levels;

    public static level_Config i;
    bool gravChoice = false;


    private void Awake()
    {
        if (i == null)
        {
            i = this;
            DontDestroyOnLoad(transform.gameObject);
        }
        else {
            Destroy(this.gameObject);
        }

        gravChoice = (Random.Range(0.0f, 1.0f) < 0.5f) ? true : false;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        //if (isAIlevel && (FindObjectOfType<Right_Player_Movement_Script>()!=false))
        //{
        //    FindObjectOfType<>();
        //}
    }

    /// <summary>
    /// this needs there to be at least one scene of each type or it may not load anything.
    /// </summary>
    public void loadCorrectScene() {
        gravChoice = !gravChoice;

        //Debug.Log("called scene loader gravchoice set to " + gravChoice);

        if ((gravChoice && withGravityScenes.Length >= 1) ) {
            gravity = true;
            int randLvel = Random.Range(0, withGravityScenes.Length);
            string level = withGravityScenes[randLvel];
            //Debug.Log("Gravchoice "+ gravChoice + " loading " + level);
            SceneManager.LoadScene(level, LoadSceneMode.Single);

        }


        if (!gravChoice && nonGravityScenes.Length >= 1) {
            //int lv = Random.Range(0, nonGravityScenes.Length);
            gravity = false;
            int randLvel = Random.Range(0, nonGravityScenes.Length);
            string level = nonGravityScenes[randLvel];
            //Debug.Log("Gravchoice " + gravChoice + " loading " + level);
            SceneManager.LoadScene(level, LoadSceneMode.Single);
        }


        
    }
}
