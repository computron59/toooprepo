﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Triangle_Initial_Spin : MonoBehaviour {

    public float torque =2500f;

	// Use this for initialization
	void Start () {
        GetComponent<Rigidbody>().AddTorque(torque * transform.forward, ForceMode.Impulse);
	}
	
	//// Update is called once per frame
	//void Update () {
		
	//}
}
