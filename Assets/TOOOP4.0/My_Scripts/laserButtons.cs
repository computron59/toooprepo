﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class laserButtons : MonoBehaviour {

    //public bool LPlayerLaser = false;
    //public bool RPlayerLaser = false;

    private touchReceiver tr;

    bool AIisActive;

    // Use this for initialization
    void Start () {
        tr = FindObjectOfType<touchReceiver>();
        AIisActive= (FindObjectOfType<Right_PlayerAI>().isActiveAndEnabled )? true: false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //pipe directly to shield and laser?
    public void L_laserOn() { tr.LPlayerLaser = true; }
    public void L_laserOff() { tr.LPlayerLaser = false; }
    public void R_laserOn() {
        if (!AIisActive) {
            tr.RPlayerLaser = true;
        }
    }
    public void r_laserOff() {
        if (!AIisActive)
        {
            tr.RPlayerLaser = false;
        }
        //tr.RPlayerLaser = ;
    }

}
