﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camera_shaker : MonoBehaviour {


    public float shake = 0;
    private float shakedecrease = 22;
    private float shakeamount = 0.5f;



	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (shake >0)
        {
            transform.localPosition = new Vector3(Random.Range(-15,15)*shakeamount, Random.Range(-15,15)*shakeamount,-350);

            shake -= Time.deltaTime * shakedecrease;
        }
        else
        {
            transform.localPosition = new Vector3(0,0,-350);
            shake = 0.0f;
        }

	}
}
