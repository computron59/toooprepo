﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RightLaser : MonoBehaviour {

    private level_Config configObj;


    //a public variable to tinker with the impulse
    private float laserForce= 18000;
    public float laser_flapForce = 2000;
    public long laser_TriangleForce = 6000000000;
    public long laser_FlopForce = 15000000;

    //audio
    private AudioSource source;

    public AudioClip laser;

    private LineRenderer LR;
    private GameObject hitobject;
    private GameObject right_hit_effect;
    private GameObject RmuzzleFX;
    private GameObject ball;

    private int sideofcentreobj = 1;


    // the bool from the shield script
    private bool laseronPrivate;
    public bool Rlaseron
    {
        get
        {
            return laseronPrivate;
        }
        set
        {
            laseronPrivate = value;
        }
    }

    // the bool from the AI script
    private bool AIlaseronPrivate;
    public bool AIRlaseron
    {
        get
        {
            return AIlaseronPrivate;
        }
        set
        {
            AIlaseronPrivate = value;
        }
    }





    private void Start()
    {
        configObj = FindObjectOfType<level_Config>();


        source = GetComponent<AudioSource>();

        //ball = GameObject.FindGameObjectWithTag("ball");

        RmuzzleFX = GameObject.FindGameObjectWithTag("R_Muzzle");
        RmuzzleFX.SetActive(false);
        hitobject = GameObject.FindGameObjectWithTag("R_LaserHitObj");
        right_hit_effect = GameObject.FindGameObjectWithTag("RightHitEffect");
        right_hit_effect.SetActive(false);
        LR = GetComponent<LineRenderer>();
        LR.enabled = false;
    }

    void Update()
    {
        if (transform.position.y < 0)
        {
            sideofcentreobj = -1;
        }
        else
        {
            sideofcentreobj = 1;
        }
        //some noise on the linerenderer
        //LR.startWidth=(Mathf.PerlinNoise(Time.time,0)*5)+3;


        //the lasers raycast
        RaycastHit RightLaserHit;

        if (!configObj.autoLasers || GameObject.FindGameObjectWithTag("ball") == false)
        {

            if (Physics.Raycast(transform.position, Vector3.left, out RightLaserHit, 15000))
            {
                hitobject.transform.position = new Vector3(RightLaserHit.point.x,
                transform.position.y, transform.position.z);

                //the laser-ball impulse
                if (RightLaserHit.transform.gameObject.tag == "ball" && (laseronPrivate || AIlaseronPrivate))
                {
                    RightLaserHit.transform.gameObject.GetComponent<Rigidbody>().AddForce(laserForce * Vector3.left * Time.deltaTime);
                }
                else if ((RightLaserHit.transform.gameObject.tag == "Lsonic00" || RightLaserHit.transform.gameObject.tag == "Rsonic00"
                   || RightLaserHit.transform.gameObject.tag == "LMegasonic" || RightLaserHit.transform.gameObject.tag == "RMegasonic") && laseronPrivate)
                {
                    RightLaserHit.transform.gameObject.GetComponent<sonic_generic_script>().popsonic = true;
                }
                //the laser-flap inpulse
                else if (RightLaserHit.transform.gameObject.tag == "flap" && laseronPrivate)
                {
                    RightLaserHit.transform.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(RightLaserHit.transform.position, laser_flapForce * Vector3.left);
                }

                //the laser-triangle inpulse
                else if (RightLaserHit.transform.gameObject.tag == "rotating_tri" && laseronPrivate)
                {
                    RightLaserHit.transform.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(RightLaserHit.transform.position, laser_TriangleForce * Vector3.left * sideofcentreobj);
                }
                //the laser-flop impulse
                else if (RightLaserHit.transform.gameObject.tag == "flop" && laseronPrivate)
                {
                    RightLaserHit.transform.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(RightLaserHit.transform.position, laser_FlopForce * Vector3.left * sideofcentreobj);
                }

            }
            else
            {
                hitobject.transform.position = new Vector3(transform.position.x - 15000,
                transform.position.y, transform.position.z);
            }
        }
        else if (GameObject.FindGameObjectWithTag("ball") != false && configObj.autoLasers)
        {
            GameObject ballObj = GameObject.FindGameObjectWithTag("ball");
            Vector3 dir = (ballObj.transform.position - transform.position).normalized;
            if (Physics.Raycast(transform.position, dir, out RightLaserHit, 15000) && laseronPrivate)
            {
                hitobject.transform.position = RightLaserHit.point;
                if (RightLaserHit.transform.tag == "ball")
                {
                    
                    ballObj.GetComponent<Rigidbody>().AddForce(laserForce * dir * Time.deltaTime);
                }

                ///////////////////////laser interactions
                //the laser-flap inpulse
                else if (RightLaserHit.transform.gameObject.tag == "flap" && laseronPrivate)
                {
                    RightLaserHit.transform.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(RightLaserHit.transform.position, laser_flapForce * dir);
                }

                //the laser-triangle inpulse
                else if (RightLaserHit.transform.gameObject.tag == "rotating_tri" && laseronPrivate)
                {
                    RightLaserHit.transform.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(RightLaserHit.transform.position, laser_TriangleForce * dir * sideofcentreobj);
                }
                //the laser-flop impulse
                else if (RightLaserHit.transform.gameObject.tag == "flop" && laseronPrivate)
                {
                    RightLaserHit.transform.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(RightLaserHit.transform.position, laser_FlopForce * dir * sideofcentreobj);
                }
                //////////////////////////////////////////


            }


        }


        LR.SetPosition(0, new Vector3(transform.position.x - 10, transform.position.y, transform.position.z));
        LR.SetPosition(1, hitobject.transform.position);



        if (laseronPrivate || AIlaseronPrivate)
        {
            source.volume = 0.01f;
            source.PlayOneShot(laser);
            hitobject.GetComponent<ParticleSystem>().Play();
            right_hit_effect.SetActive(true);
            RmuzzleFX.SetActive(true);
            LR.enabled = true;
        }
        else
        {
            if (hitobject.GetComponent<ParticleSystem>().isPlaying)
            {
                hitobject.GetComponent<ParticleSystem>().Stop();
            }

            source.Stop();
            RmuzzleFX.SetActive(false);
            LR.enabled = false;
            right_hit_effect.SetActive(false);
        }
    }
}
