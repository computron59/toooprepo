﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class level_music : MonoBehaviour {

    private AudioSource source;
    public AudioClip[] levelmusic;
    int musicChoice;

    // Use this for initialization
    void Start () {
        
        source = GetComponent<AudioSource>();
        source.volume = 0.1f;
        musicChoice = Random.Range(0, levelmusic.Length - 1);
        
	}
	
	// Update is called once per frame
	void Update () {
        if (source.isPlaying == false)
        {
            source.PlayOneShot(levelmusic[musicChoice]);
        }
	}
}
