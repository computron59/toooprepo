﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ball_Gravity_Script : MonoBehaviour {

    public bool gravity_On = true;

    [SerializeField]
    public float gravityStrength = 60f;

	void FixedUpdate () {
        if (gravity_On) {
           GetComponent<Rigidbody>().AddForce(new Vector3(0,-gravityStrength,0), ForceMode.Acceleration);
        }
	}
}
