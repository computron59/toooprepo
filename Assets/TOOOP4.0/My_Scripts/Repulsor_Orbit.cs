﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Repulsor_Orbit : MonoBehaviour {

    public float orbitSpeed = 25f;
	
	// Update is called once per frame
	void Update () {
        transform.RotateAround(Vector3.zero, Vector3.forward, orbitSpeed * Time.deltaTime);
	}
}
