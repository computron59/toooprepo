﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RPlayerCrashNBurn : MonoBehaviour {
    //public GameObject explosion;

    // Use this for initialization
    void Start()
    {
        Destroy(GameObject.FindGameObjectWithTag("R_Aim_Fire_Obj"));
        StartCoroutine(destroyme(1.6f));
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.Rotate(-7, 10, -5);
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(0, 0, 0), 1.0f);
    }

    IEnumerator destroyme(float crashtime)
    {
        yield return new WaitForSeconds(crashtime);
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<camera_shaker>().shake = 8;

        Instantiate(Resources.Load("explosion"), transform.position, Quaternion.identity);
        Handheld.Vibrate();
        Destroy(this.gameObject);
    }
}