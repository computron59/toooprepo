﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class megaBounceScript : MonoBehaviour {

    
    public float bounceForce = 120f;
    public bool isRound = false;

    Transform aimer;
    Vector3 dir;

    // Use this for initialization
    void Start () {
        aimer = transform.Find("aimer");
	}
	
	// Update is called once per frame
	void Update () {

	}

    void OnCollisionEnter(Collision col) {
        if (col.transform.gameObject.tag == "ball") {

            //float scaleBounce = 1/col.rigidbody.velocity.y;

            if (!isRound)
            {
                dir = (aimer.position - transform.position).normalized;
                col.rigidbody.AddForce(dir * bounceForce, ForceMode.Impulse);
                Debug.Log("Sq Collision");
            }
            else {
                dir = (col.transform.position - transform.position).normalized;
                col.rigidbody.AddForce(dir * bounceForce, ForceMode.Impulse);
            }
        }
    }
}
