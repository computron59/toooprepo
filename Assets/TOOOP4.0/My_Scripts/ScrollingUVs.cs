﻿using UnityEngine;
using System.Collections;

public class ScrollingUVs : MonoBehaviour
{
    public int materialIndex = 0;
    public Vector2 uvAnimationRateOne = new Vector2(1.0f, 0.0f);
    public Vector2 uvAnimationRateTwo = new Vector2(1.0f, 0.0f);
    public Vector2 uvAnimationRateTre = new Vector2(1.0f, 0.0f);
    public string textureName = "_MainTex";
    private Renderer rend;

    Vector2 uvOffsetOne = Vector2.zero;
    Vector2 uvOffsetTwo = Vector2.zero;
    Vector2 uvOffsetTre = Vector2.zero;


    int materialNumber = 0;

    void Start()
    {
        rend = GetComponent<Renderer>();
        materialNumber = rend.materials.Length;
    } 

    void LateUpdate()
    {
        if (materialNumber>=1) {
            uvOffsetOne += (uvAnimationRateOne * Time.deltaTime);
        }

        if (materialNumber >= 2)
        {
            uvOffsetTwo += (uvAnimationRateTwo * Time.deltaTime);
        }

        if (materialNumber >= 3)
        {
            uvOffsetTre += (uvAnimationRateTwo * Time.deltaTime);
        }

        if ( rend.enabled)
        {
            if (materialNumber >= 1)
            {
                rend.materials[0].SetTextureOffset(textureName, uvOffsetOne);
            }

            if (materialNumber >= 2)
            {
                rend.materials[1].SetTextureOffset(textureName, uvOffsetTwo);
            }

            if (materialNumber >= 3)
            {
                rend.materials[2].SetTextureOffset(textureName, uvOffsetTre);
            }
        }
    }
}