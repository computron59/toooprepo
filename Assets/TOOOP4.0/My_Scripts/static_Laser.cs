﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class static_Laser : MonoBehaviour {

    public bool isAPeriodicLaser = false;
    public bool staticLaseron = true;
    public float period = 0.85f;
    float timer = 0;

    GameObject muzzleFX;

    ParticleSystem ps0;
    ParticleSystem ps1;
    ParticleSystem ps2;
    ParticleSystem ps3;

    //public GameObject testhit;
    LineRenderer LR;
    public float width = 20f;
    public float laserForce = 150f;
    GameObject LhitFXObj;
    GameObject RhitFXObj;

    GameObject aimer;
    //public LayerMask wallsNplayersOnly;

    public bool init= false;

    // Use this for initialization
    void Start () {
        LR = GetComponent<LineRenderer>();
        
        //LR.SetPosition(1, hitobject.transform.position);
        

        LhitFXObj = Instantiate( Resources.Load("LStaticLaserHitObj")) as GameObject;
        RhitFXObj = Instantiate( Resources.Load("RStaticLaserHitObj")) as GameObject;
        ps2 = LhitFXObj.GetComponent<ParticleSystem>();
        ps3 = RhitFXObj.GetComponent<ParticleSystem>();

        Debug.Log("Objects exist as ; "  + LhitFXObj + " and " + RhitFXObj);
        Debug.Log("Objects are at " + LhitFXObj.transform.position + " and " + RhitFXObj.transform.position );

        muzzleFX = transform.Find("R_MuzzleFX (1)").gameObject;

        aimer = this.transform.Find("aimObj").gameObject;

        ps0 = transform.Find("staticLaserHitObj (1)").gameObject.GetComponent<ParticleSystem>();
        ps1 = transform.Find("staticLaserHitObj (2)").gameObject.GetComponent<ParticleSystem>();

    }
	
	// Update is called once per frame
	void Update () {

        timer += Time.deltaTime;


        if (timer >= period && isAPeriodicLaser) {
            timer = 0;
            staticLaseron = !staticLaseron;
        }



        if (isAPeriodicLaser && !staticLaseron)
        {
            muzzleFX.SetActive(false);
            ps0.Stop();
            ps1.Stop();
            ps2.Stop();
            ps3.Stop();
            
            LR.enabled = false;
        }
        else {
            muzzleFX.SetActive(true);
            ps0.Play();
            ps1.Play();
            ps2.Play();
            ps3.Play();
            
            LR.enabled = true;
        }

        Vector3 lineOfSight = (aimer.transform.position - this.transform.position).normalized;

        // two raycasts
        RaycastHit LhitRayObj, RhitRayObj;
        LR.SetPosition(0, new Vector3(transform.position.x, transform.position.y, transform.position.z));
        LR.SetPosition(1, transform.position+lineOfSight*Screen.width*1.5f);



        if (!isAPeriodicLaser || staticLaseron) {

            if (Physics.Raycast(transform.position - Vector3.right * width / 2, lineOfSight, out LhitRayObj, Screen.width * 3 / 2))
            {
                LhitFXObj.transform.position = LhitRayObj.point + lineOfSight * 5f;
                //testhit.transform.position = LhitRayObj.point;
                if (LhitRayObj.transform.gameObject.tag == "ball")
                {
                    LhitRayObj.transform.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(lineOfSight * laserForce, LhitRayObj.point);

                }

            }
            if (Physics.Raycast(transform.position + Vector3.right * width / 2, lineOfSight, out RhitRayObj, Screen.width * 3 / 2))
            {
                RhitFXObj.transform.position = RhitRayObj.point + lineOfSight * 5f;
                if (RhitRayObj.transform.gameObject.tag == "ball")
                {
                    RhitRayObj.transform.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(lineOfSight * laserForce, RhitRayObj.point);

                }
            }
        }

    }
}
/*using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RightLaser : MonoBehaviour {

    //a public variable to tinker with the impulse
    public float laserForce= 200;

    public long laser_TriangleForce = 6000000000;
    public long laser_FlopForce = 15000000;

    //audio
    private AudioSource source;

    public AudioClip laser;

    private LineRenderer LR;
    private GameObject hitobject;
    private GameObject right_hit_effect;
    private GameObject RmuzzleFX;
    private GameObject ball;

    private int sideofcentreobj = 1;


    // the bool from the shield script
    private bool laseronPrivate;
    public bool Rlaseron
    {
        get
        {
            return laseronPrivate;
        }
        set
        {
            laseronPrivate = value;
        }
    }

    // the bool from the AI script
    private bool AIlaseronPrivate;
    public bool AIRlaseron
    {
        get
        {
            return AIlaseronPrivate;
        }
        set
        {
            AIlaseronPrivate = value;
        }
    }





    private void Start()
    {
        source = GetComponent<AudioSource>();

        //ball = GameObject.FindGameObjectWithTag("ball");

        RmuzzleFX = GameObject.FindGameObjectWithTag("R_Muzzle");
        RmuzzleFX.SetActive(false);
        hitobject = GameObject.FindGameObjectWithTag("R_LaserHitObj");
        right_hit_effect = GameObject.FindGameObjectWithTag("RightHitEffect");
        right_hit_effect.SetActive(false);
        LR = GetComponent<LineRenderer>();
        LR.enabled = false;
    }

    void Update()
    {
        if (transform.position.y < 0)
        {
            sideofcentreobj = -1;
        }
        else
        {
            sideofcentreobj = 1;
        }
        //some noise on the linerenderer
        //LR.startWidth=(Mathf.PerlinNoise(Time.time,0)*5)+3;


        //the lasers raycast
        RaycastHit RightLaserHit;
        if (Physics.Raycast(transform.position, Vector3.left, out RightLaserHit, 15000))
        {
            hitobject.transform.position = new Vector3(RightLaserHit.point.x,
            transform.position.y, transform.position.z);

            //the laser-ball impulse
            if (RightLaserHit.transform.gameObject.tag == "ball" && (laseronPrivate || AIlaseronPrivate))
            {
                RightLaserHit.transform.gameObject.GetComponent<Rigidbody>().AddForce(laserForce*Vector3.left);
            }
            else if ((RightLaserHit.transform.gameObject.tag == "Lsonic00" || RightLaserHit.transform.gameObject.tag == "Rsonic00"
               || RightLaserHit.transform.gameObject.tag == "LMegasonic" || RightLaserHit.transform.gameObject.tag == "RMegasonic") && laseronPrivate)
            {
                RightLaserHit.transform.gameObject.GetComponent<sonic_generic_script>().popsonic = true;
            }
            //the laser-flap inpulse
            else if (RightLaserHit.transform.gameObject.tag == "flap" && laseronPrivate)
            {
                RightLaserHit.transform.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(RightLaserHit.transform.position, laserForce * Vector3.left);
            }

            //the laser-triangle inpulse
            else if (RightLaserHit.transform.gameObject.tag == "rotating_tri" && laseronPrivate)
            {
                RightLaserHit.transform.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(RightLaserHit.transform.position, laser_TriangleForce * Vector3.left* sideofcentreobj);
            }
            //the laser-flop impulse
            else if (RightLaserHit.transform.gameObject.tag == "flop" && laseronPrivate)
            {
                RightLaserHit.transform.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(RightLaserHit.transform.position, laser_FlopForce * Vector3.left* sideofcentreobj);
            }

        }
        else
        {
            hitobject.transform.position = new Vector3(transform.position.x - 15000,
            transform.position.y, transform.position.z);
        }



        LR.SetPosition(0, new Vector3(transform.position.x - 10, transform.position.y, transform.position.z));
        LR.SetPosition(1, hitobject.transform.position);



        if (laseronPrivate || AIlaseronPrivate)
        {
            source.volume = 0.01f;
            source.PlayOneShot(laser);
            hitobject.GetComponent<ParticleSystem>().Play();
            right_hit_effect.SetActive(true);
            RmuzzleFX.SetActive(true);
            LR.enabled = true;
        }
        else
        {
            if (hitobject.GetComponent<ParticleSystem>().isPlaying)
            {
                hitobject.GetComponent<ParticleSystem>().Stop();
            }

            source.Stop();
            RmuzzleFX.SetActive(false);
            LR.enabled = false;
            right_hit_effect.SetActive(false);
        }
    }
}
*/
