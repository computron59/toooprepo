﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

// because the energy is the shield size laser pop and sonic are all set up here.
public class Shield_Controller : MonoBehaviour {

    public Color Lslider0, Lslider1;
    Slider LHth;

    public float respawnFraction = 0.8f;    // the fraction of full shields on respawn.

    private float megaspacing = 15f;

    private float supersonic_held_down = 0;
    private float supersonic_held_downLimit = 0.55f;
    private float sonicspeed = 60f;

    public bool madmode = false; // this is a bool that gets turned on if the aimer is activated. It is not turned off. 

    private GameObject spawnableballprefab;//The spawnable ball.
    private float spawnimpulse = 00f;

    const float PI = 3.14592217f;

    private AudioSource source;
    
    public AudioClip popaudio;
    
    //aimer rotation stuff.
    private float orbitDistance = 40f;

    //here we have two rotation speeds, one slow one at the front and a faster one for the back.
    private float aimerRotateSpeed0 = 1.4f;
    private float aimerRotateSpeed1 = 5.8f;
    private float theta= 0;

    //these are the two angles to be fast between
    private float fastlimitlow = PI / 2;
    private float fastlimithight = 3 * PI / 2;

    //these are the two angles to be slow between
    // the speed varys between the two sets of limits.
    private float slowlimitlow = PI / 3;
    private float slowlimithigh = 5 * PI / 3;

    // the bool for firing balls
    public bool ballfireon= false;
    public bool otherplayerready = true;



    private float shield_Default_Diameter= 150;
    private float shield_Minimum_Diameter = 50;
    private Vector3 size;
    private Vector3 sizeOfPopShield;
    private float width;
    private float popwidth;
    private float popEnergyCost=30;
    private GameObject Left_Player_popShield;
    private LeftLaser leftLaserScript;

    private GameObject aimer;// The ball fire aimer.



    // this is true instantly if a pop is fired when it is possible
    private bool pop = false;

    // this is true during the pop.
    private bool popping = false;

    // this is the link to the laser line renderer
    private GameObject laserObject;

    touchReceiver touchObj;

    void Start() {

        //ColorUtility.TryParseHtmlString(hexString, out Lslider0);
        //ColorUtility.TryParseHtmlString(hexString, out Lslider1);

        LHth = GameObject.FindGameObjectWithTag("lsheildbar").GetComponent<Slider>();

        touchObj = FindObjectOfType<touchReceiver>();

        source = GetComponent<AudioSource>();

        
        Left_Player_popShield = GameObject.FindGameObjectWithTag("L_Player_PopShield");

        laserObject = GameObject.FindGameObjectWithTag("L_Player_Container");
        leftLaserScript = laserObject.GetComponent<LeftLaser>();
    }


    void Update() {

        // SUPERSONICS NOT USED IN THIS BUILD

        //if (Input.GetButton("sonicL"))
        //{
        //    supersonic_held_down += Time.deltaTime;
        //    if (supersonic_held_down >= supersonic_held_downLimit)
        //    {
        //        supersonic_held_down = 0;

        //        Debug.Log("SuperSonic Triggered");
        //        GameObject sonicRB00;
        //        GameObject sonicRB01;
        //        GameObject sonicRB02;
        //        GameObject sonicRB03;
        //        GameObject sonicRB04;

        //        sonicRB00 = Instantiate(Resources.Load("L_MegaSonic_Container00"), new Vector3(this.transform.position.x + Left_Player_popShield.GetComponent<MeshRenderer>().bounds.size.x / 2 + 15,
        //            this.transform.position.y+megaspacing*2, 0), Quaternion.identity) as GameObject;
        //        sonicRB01 = Instantiate(Resources.Load("L_MegaSonic_Container00"), new Vector3(this.transform.position.x + Left_Player_popShield.GetComponent<MeshRenderer>().bounds.size.x / 2 + 15,
        //            this.transform.position.y+megaspacing, 0), Quaternion.identity) as GameObject;
        //        sonicRB02 = Instantiate(Resources.Load("L_MegaSonic_Container00"), new Vector3(this.transform.position.x + Left_Player_popShield.GetComponent<MeshRenderer>().bounds.size.x / 2 + 15,
        //            this.transform.position.y, 0), Quaternion.identity) as GameObject;
        //        sonicRB03 = Instantiate(Resources.Load("L_MegaSonic_Container00"), new Vector3(this.transform.position.x + Left_Player_popShield.GetComponent<MeshRenderer>().bounds.size.x / 2 + 15,
        //            this.transform.position.y-megaspacing, 0), Quaternion.identity) as GameObject;
        //        sonicRB04 = Instantiate(Resources.Load("L_MegaSonic_Container00"), new Vector3(this.transform.position.x + Left_Player_popShield.GetComponent<MeshRenderer>().bounds.size.x / 2 + 15,
        //            this.transform.position.y-2*megaspacing, 0), Quaternion.identity) as GameObject;

        //        sonicRB00.GetComponent<Rigidbody>().velocity = Vector3.right * sonicspeed;
        //        sonicRB01.GetComponent<Rigidbody>().velocity = Vector3.right * sonicspeed;
        //        sonicRB02.GetComponent<Rigidbody>().velocity = Vector3.right * sonicspeed;
        //        sonicRB03.GetComponent<Rigidbody>().velocity = Vector3.right * sonicspeed;
        //        sonicRB04.GetComponent<Rigidbody>().velocity = Vector3.right * sonicspeed;

        //        transform.localScale *= 0.910f;
        //    }
        //}else
        //{
        //    supersonic_held_down = 0;
        //}

        //if (Input.GetButtonUp("sonicL") && width > shield_Minimum_Diameter)
        //{
        //    GameObject sonicRB;
        //    sonicRB = Instantiate(Resources.Load("L_Sonic_Container00"), new Vector3(this.transform.position.x+ Left_Player_popShield.GetComponent<MeshRenderer>().bounds.size.x/2+15, 
        //        this.transform.position.y,0), Quaternion.identity) as GameObject;
        //    sonicRB.GetComponent<Rigidbody>().velocity = Vector3.right*sonicspeed;


        //    transform.localScale *= 0.920f;
        //}


        // MAD MODE 
        if (!madmode&& ballfireon)//this recognses that the aimer has been used and sets mad mode, ie no spawning.
        {
            madmode = true;
        }


        if (ballfireon)
        {
            if (GameObject.FindGameObjectWithTag("L_Aim_Fire_Obj")==null)
            {
                aimer = Instantiate(Resources.Load("L_Aim_And_Fire_Object")) as GameObject;
            }
            
            rotateAimer();
        }
        else
        {
            if (GameObject.FindGameObjectWithTag("L_Aim_Fire_Obj") != null)
            {
                Destroy(aimer);
            }
        }


        size = new Vector3(GetComponent<MeshRenderer>().bounds.size.x,
                               GetComponent<MeshRenderer>().bounds.size.y,
                               GetComponent<MeshRenderer>().bounds.size.z);

        sizeOfPopShield = new Vector3(Left_Player_popShield.GetComponent<MeshRenderer>().bounds.size.x,
                            Left_Player_popShield.GetComponent<MeshRenderer>().bounds.size.y,
                            Left_Player_popShield.GetComponent<MeshRenderer>().bounds.size.z);

        // this can be considered as the current shield energy
        width = size.x;

        // this is the diameter of the popping shield
        popwidth = sizeOfPopShield.x;

        // if the laser button is pressed and there is energy left
        if ((Input.GetButton("LaserL") || touchObj.LPlayerLaser ) && width > shield_Minimum_Diameter) {
            

            transform.localScale *= 0.992f;
            leftLaserScript.laseron = true;
        }
        else
        {
            leftLaserScript.laseron = false;
        }

        //if the pop button is pressed and there is enough energy for a pop
        if ((Input.GetButtonDown("PopL") || touchObj.LPlayerTouchPop) && (width - popEnergyCost) > shield_Minimum_Diameter && otherplayerready) {
            if (touchObj.LPlayerTouchPop) {
                touchObj.LPlayerTouchPop = false;
            }


            pop = true;
            source.volume = 0.5f;
            source.PlayOneShot(popaudio);
        } else if (touchObj.LPlayerTouchPop) {
            touchObj.LPlayerTouchPop = false;
        }

        // this reinflates the shield
        if (width < shield_Default_Diameter) { transform.localScale *= 1.003f; }

        // this keeps the popshield inactive and scaled whilst not popping
        if (!pop && !popping) {
            Left_Player_popShield.SetActive(false);
            Left_Player_popShield.transform.localScale = transform.localScale;
        }

        // this pops the shield and sets the popping to true
        if (pop)
        {
            //this fires a ball if the bool is true.
            if (ballfireon)// do i want to instantiate with a spin?
            {
                Vector3 targetdir = (aimer.transform.position- laserObject.transform.position).normalized;
                Vector3 spawnpoint = laserObject.transform.position + targetdir * GetComponent<SphereCollider>().radius*1.15f;

                spawnableballprefab = Instantiate(Resources.Load("Ball_Container"), spawnpoint, Quaternion.identity )as GameObject;
                spawnableballprefab.GetComponent<Rigidbody>().AddForce(targetdir*spawnimpulse, ForceMode.Impulse);
                if (GameObject.FindGameObjectWithTag("congfigObj").GetComponent<level_Config>().gravity)
                {
                    spawnableballprefab.GetComponent<ball_Gravity_Script>().gravity_On = true;
                }
            }

            if (ballfireon == false)
            {
                transform.localScale *= 0.60f;
            }else
            {
                transform.localScale *= 0.30f;
            }



            Left_Player_popShield.transform.localScale = transform.localScale;
            popping = true;
            pop = false;

        }


        if (popping)
        {
            //Activates and expands the second shield
            Left_Player_popShield.SetActive(true);
            Left_Player_popShield.transform.localScale *= 1.150f;

            // destroys the shield and cleans up
            if (popwidth>270) {
                Left_Player_popShield.transform.localScale = transform.localScale;
                Left_Player_popShield.SetActive(false);
                popping = false;
            }
        }

        //shield bar update
        LHth.value = (width- shield_Minimum_Diameter) / (shield_Default_Diameter- shield_Minimum_Diameter);

        if ((width - popEnergyCost) < shield_Minimum_Diameter)
        {
            LHth.fillRect.GetComponent<Image>().color = Lslider1;
        }
        else {

            LHth.fillRect.GetComponent<Image>().color = Lslider0;

        }


        
    }


    public void resetSheild() {
        while (GetComponent<MeshRenderer>().bounds.size.x < shield_Default_Diameter*respawnFraction) {
            transform.localScale *= 1.12f;
        }
    }

    void rotateAimer()
    {

        if (theta > 2*PI)
        {
            theta -= 2 * PI;
        }//keeps the angle sensical.


        // the slow limit , less than 60 or more than 300
        if ((theta < slowlimitlow) || theta > slowlimithigh)
        {
            theta += aimerRotateSpeed0 * Time.deltaTime;
        }
        else if (theta > slowlimitlow && theta < fastlimitlow)// intermediate speeds accelerating.
        {
            theta += (aimerRotateSpeed0 + (aimerRotateSpeed1 - aimerRotateSpeed0) * ((theta - slowlimitlow) / (fastlimitlow - slowlimitlow)))*Time.deltaTime;
        }
        else if (theta <slowlimithigh  && theta >fastlimithight )// intermediate speeds deccelerating.
        {
            theta += (  aimerRotateSpeed0 + (aimerRotateSpeed1 - aimerRotateSpeed0) * 
                ( (theta-slowlimithigh)/(fastlimithight-slowlimithigh)))*Time.deltaTime;
        }




        else
        {
            theta += aimerRotateSpeed1 * Time.deltaTime;
        }



        aimer.transform.position = laserObject.transform.position + new Vector3(orbitDistance * Mathf.Cos(theta), orbitDistance * Mathf.Sin(theta), 0);
        
       
    }

}
