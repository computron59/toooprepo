﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuSliderScript : MonoBehaviour {

    private GameObject handle;
    RectTransform canvasRect;
    float canvasScale;
    public float distToNextScreen;
    public AnimationCurve animationCurve;
    float movementDist = 300f;
    public float movementBaseSpeed = 10f;
    bool movingRight = false;
    bool movingLeft = false;
    float start;
    GameObject basicBlurb;
    GameObject modeBlurb;
    AudioSource source;
    public AudioClip slide;
    bool isleft = false;


    // Use this for initialization
    void Start() {
        source = this.GetComponent<AudioSource>();
        basicBlurb = GameObject.Find("AI_general_blurb");
        modeBlurb = GameObject.Find("single_player_blurb");
        basicBlurb.SetActive(false);
        handle = GameObject.FindGameObjectWithTag("menuTag");
        start = handle.transform.position.x;
        canvasRect = GameObject.FindObjectOfType<Canvas>().GetComponent<RectTransform>();
        canvasScale = canvasRect.localScale.x;
        //distToNextScreen = GameObject.FindGameObjectWithTag("aiButtonContainer").transform.position.x;
        //Debug.Log("Dist " + distToNextScreen);
        movementDist = distToNextScreen * canvasScale;
	}

    // Update is called once per frame
    void FixedUpdate()
    {
        if ((handle.transform.position.x < start + movementDist) && movingLeft)
        {
            
            float scale= animationCurve.Evaluate((handle.transform.position.x-start)/movementDist);
            handle.transform.position += Vector3.right * movementBaseSpeed* scale;
            basicBlurb.SetActive(true);
            modeBlurb.SetActive(false);
        }
        if ((handle.transform.position.x >= start + movementDist) && movingLeft)
        {
            handle.transform.position = new Vector3(start+movementDist, handle.transform.position.y, handle.transform.position.z);
            isleft = true;
            movingLeft = false;
            
        }


        if ((handle.transform.position.x > start ) && movingRight)
        {
            isleft = false;
            float scale = animationCurve.Evaluate((handle.transform.position.x - start) / movementDist);
            handle.transform.position -= Vector3.right * movementBaseSpeed*scale;
            basicBlurb.SetActive(false);
            modeBlurb.SetActive(true);
            //Debug.Log("Moving Right " + movingRight + " coord is  " + handle.transform.position.x);
        } 
        if ((handle.transform.position.x <= start) && movingRight)
        {
            handle.transform.position = new Vector3(start, handle.transform.position.y, handle.transform.position.z);
            movingRight = false;
        }

    }

    public void slideLeft() {
        movingLeft = true;
        source.PlayOneShot(slide, .60f);
    }

    public void slideRight() {
        movingRight = true;
        source.PlayOneShot(slide, .60f);
    }

    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape) && isleft && !movingRight && !movingRight)
        {
            movingRight = true;
            source.PlayOneShot(slide, .60f);
        }
    }
}
