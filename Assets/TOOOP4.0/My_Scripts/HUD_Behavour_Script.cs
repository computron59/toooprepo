﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class HUD_Behavour_Script : MonoBehaviour {

    level_Config levelconfig;

    public int OpponentHearts=9;
    public int Hearts=3;
    public string leveltoload = "Rotating_Repulsors";

    private int MAXHearts = 40;


    private GameObject[] LHearts;
    private GameObject[] RHearts;

    private void Start()
    {
        levelconfig = FindObjectOfType<level_Config>();
        LHearts = new GameObject[MAXHearts];
        RHearts = new GameObject[MAXHearts];

        if(Hearts> MAXHearts) { Hearts = MAXHearts; }
        if (OpponentHearts > MAXHearts) { OpponentHearts = MAXHearts; }


        for (int i=0; i< MAXHearts;i++) {
            LHearts[i] = Instantiate(Resources.Load("Life_Image")) as GameObject;
            RHearts[i] = Instantiate(Resources.Load("Life_Image")) as GameObject;
            LHearts[i].transform.SetParent(GameObject.FindGameObjectWithTag("canvas").transform);
            RHearts[i].transform.SetParent(GameObject.FindGameObjectWithTag("canvas").transform);

            if (i > 19)
            {
                LHearts[i].transform.position = new Vector3(20 + 15 * (i-20), Screen.height - 40, -5);
                RHearts[i].transform.position = new Vector3(Screen.width- 20 - 15 * (i - 20), Screen.height - 40, -5);
            }
            else
            {
                LHearts[i].transform.position = new Vector3(20 + 15 * i, Screen.height - 20, -5);
                RHearts[i].transform.position = new Vector3(Screen.width-20 - 15 * i, Screen.height - 20, -5);
            }
            
        }
    }
    

    private void Update()
    {
        if (Hearts > MAXHearts) { Hearts = MAXHearts; }
        if (OpponentHearts > MAXHearts) { OpponentHearts = MAXHearts; }

        for (int i=0; i < MAXHearts; i++) {
            if (i >= Hearts)
            {
                LHearts[i].SetActive(false);
            }
            if (i >= OpponentHearts)
            {
                RHearts[i].SetActive(false);
            }
        }


        if (Hearts <= 0 || OpponentHearts <= 0)
        {
            //levelconfig.nextLevelToLoad = "main_menu";
            levelconfig.loadCorrectScene();
        }

    }


    //public GameObject[] HeartArray;
    //public GameObject[] OpponentHeartArray;

    //private int hearts;
    //public int Hearts
    //{
    //    get
    //    {
    //        return hearts;
    //    }
    //    set
    //    {
    //        hearts = value;
    //    }
    //}

    //private int Opponenthearts;
    //public int OpponentHearts
    //{
    //    get
    //    {
    //        return Opponenthearts;
    //    }
    //    set
    //    {
    //        Opponenthearts = value;
    //    }
    //}


    //void Start()
    //{
    //    hearts = HeartArray.Length-1;
    //    Opponenthearts = OpponentHeartArray.Length - 1;
    //}

    //// Update is called once per frame
    //void Update () {
    //() {

    //        for(int i=(HeartArray.Length-1); i >=0; i--)
    //        {
    //            if (hearts<i)
    //            {
    //            HeartArray[i].SetActive(false);
    //            }
    //        }

    //    for (int i = (OpponentHeartArray.Length - 1); i >= 0; i--)
    //    {
    //        if (Opponenthearts < i)
    //        {
    //            OpponentHeartArray[i].SetActive(false);
    //        }
    //    }

    //    if (hearts==0 || Opponenthearts==0 )
    //    {
    //        SceneManager.LoadScene("main_menu");// You wanna put in a win/lose Screen here.
    //    }
    //}
}
