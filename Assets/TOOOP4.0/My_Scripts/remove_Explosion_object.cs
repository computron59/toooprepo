﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class remove_Explosion_object : MonoBehaviour {

	// Use this for initialization
	void Start () {
        StartCoroutine(removeMe(5.0f));
	}
	
	IEnumerator removeMe(float wait)
    {
        yield return new WaitForSeconds(wait);
        Destroy(this.gameObject);
    }
}
