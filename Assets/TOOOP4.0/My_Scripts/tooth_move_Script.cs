﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tooth_move_Script : MonoBehaviour {

    public float toothMoveSpeed = 10f;
    int dir = -1;

	// Update is called once per frame
	void Update () {

        if (transform.position.y >= 100 && dir == 1) {
            dir = -1;
        } else if (transform.position.y <= -100 && dir==-1) {
            dir = +1;

        }

        transform.position = new Vector3(transform.position.x,transform.position.y+ dir*toothMoveSpeed*Time.deltaTime,transform.position.z);
	}
}
