﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class setupScene : MonoBehaviour {

	// initialises the scene from the persistent config object.
	void Awake () {
        level_Config lc =  FindObjectOfType<level_Config>();
        if (lc.isAIlevel)
        {
            GameObject[] Rbuts = GameObject.FindGameObjectsWithTag("RButtons");
            foreach (GameObject rb in Rbuts) {
                rb.SetActive(false);
            }
            FindObjectOfType<Right_Player_Movement_Script>().enabled = false;
        }
        else {
            FindObjectOfType<Right_PlayerAI>().enabled = false;
        }
	}
	
	
}
