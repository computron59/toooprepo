﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class instructionsBehaviour : MonoBehaviour {

   
    //Color mentalText;
    float timer;
    int sign=1;

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        timer += (sign) * Time.deltaTime * 340;
        if (timer >= 254 && sign == 1)
        {
            sign = -1;
        }
        else if (timer <= 1 && sign == -1)
        {
            sign = +1;
        }

        GameObject[] flashytext  = GameObject.FindGameObjectsWithTag("metalText");

        Color mentalText = new Color((timer / 255) , (timer / 255)*0.3f, (timer / 255) * 0.3f, 0.8f);
        foreach (GameObject g in flashytext) {
            g.GetComponent<Text>().color = mentalText;
        }
        
    }
}
