﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class Level_choice : MonoBehaviour {

    //public float buttonmovespeed0 = 10;
    //int dir0=1, dir1=1;
    //float buttonrestcentre = 260;
    //float buttonselectcentre = 310;
    float timer = 0;/*, flashytimer=0;*/
    //public string name;
    level_Config levelconfig;
    public GameObject[] AIButtons;
    //public GameObject[] ModeButtons;
    //GameObject GoButton;
    //public Color  selected, notselected, notselectedtext, selectedtext, greyedout, greydouttext, chosengreyedout, chosengreydouttext;
    Color mental; //, inverseMental, inversegreenbluemental, greenyglow;
    int sign = +1;//, flashysign=+1;
    //GameObject reubutton;
    //GameObject selectedModeButton;
    //GameObject selectedAIButton;
    int AIlevel;
    GameObject opponentSign;
    public Color greyedout, greydouttext;

    // Use this for initialization
    void Start() {
        if (!PlayerPrefs.HasKey("AI_level"))
        {
            PlayerPrefs.SetInt("AI_level", 3);
            
        }

        AIlevel = 3;//PlayerPrefs.GetInt("AI_level");
        

        disableAIs();
        opponentSign = GameObject.Find("opponentchoicetext");

        levelconfig = FindObjectOfType<level_Config>();
        AIButtons = GameObject.FindGameObjectsWithTag("AI_Button");
        ////reubutton = GameObject.Find("reubutton");


        ////selectedModeButton = ModeButtons[0];
        //GoButton = GameObject.FindGameObjectWithTag("go_button");

        ////selectedAIButton = AIButtons[0];
        //levelconfig.isAIlevel = true;
        //selectthisbutton(AIButtons[0]);
        //levelconfig.isAIlevel = false;
        //seleectmodeButton(ModeButtons[0]);
        //greyOutTheAI();
    }

    private void Update()
    {
        timer += (sign) * Time.deltaTime * 340;
        if (timer >= 254 && sign == 1)
        {
            sign = -1;
        }
        else if (timer <= 1 && sign == -1)
        {
            sign = +1;
        }

        ////second timer flashy 
        //flashytimer += (flashysign) * Time.deltaTime * 1050;
        //if (flashytimer >= 254 && flashysign == 1)
        //{
        //    flashysign = -1;
        //}
        //else if (flashytimer <= 1 && flashysign == -1)
        //{
        //    flashysign = +1;
        //}



        mental = new Color((timer / 255)*0.6f, (timer / 255), (timer / 255)*0.6f, 0.8f);
        //inverseMental = new Color((1-(timer / 255)), (1 - (timer / 255)), (1 - (timer / 255)), 0.8f);
        //inversegreenbluemental = new Color((1 - (timer / 255)) * 0.6f, (1 - (timer / 255)), (1 - (timer / 255)), 0.8f);
        //greenyglow = new Color((flashytimer / 255)*0.05f, (flashytimer / 255)*0.35f, (flashytimer / 255)*00.05f, 1.0f);
        ////GoButton.GetComponentInChildren<Text>().color = mental;
        ////GoButton.GetComponentInChildren<Image>().color = inversegreenbluemental;
        ////selectedModeButton.GetComponent<Button>().GetComponent<Image>().color = selected;
        ////selectedModeButton.GetComponentInChildren<Text>().color = greenyglow;

        //if (levelconfig.isAIlevel)
        //{
        //    selectedAIButton.GetComponent<Button>().GetComponent<Image>().color = selected;
        //    selectedAIButton.GetComponentInChildren<Text>().color = selectedtext;
        //}

        opponentSign.GetComponentInChildren<Text>().color = mental;
        //moveModeButtons();
    }

//    public void TestLoadScene()
//    {
//        //levelconfig.nextLevelToLoad = name;
//        levelconfig.loadCorrectScene();
//    }

    //    public void selectthisbutton(GameObject B) {
    //        if (levelconfig.isAIlevel) {
    //            for (int i = 0; i < AIButtons.Length; i++) {
    //                if (AIButtons[i] != B)
    //                {
    //                    AIButtons[i].GetComponent<Button>().GetComponent<Image>().color = notselected;
    //                    AIButtons[i].GetComponentInChildren<Text>().color = notselectedtext;

    //                }
    //                else
    //                {
    //                    selectedAIButton = AIButtons[i];
    //                    levelconfig.setAIConfig = (AItype)i;

    //                }
    //            }
    //        }



    //        //foreach (GameObject b in AIButtons) {
    //        //    if (b != B)
    //        //    {
    //        //        b.GetComponent<Button>().GetComponent<Image>().color = notselected;
    //        //        b.GetComponentInChildren<Text>().color = notselectedtext;

    //        //    }
    //        //    else {
    //        //        selectedAIButton = b;
    //        //        levelconfig.setAIConfig = (AItype)i;
    //        //        Debug.Log("Set AI to " + (AItype)i);
    //        //    }
    //        //}
    //    }

    //    public void seleectmodeButton(GameObject B) {
    //        for (int i=0; i<ModeButtons.Length; i++)
    //        {
    //            if (ModeButtons[i] != B)
    //            {


    //                ModeButtons[i].GetComponent<Button>().GetComponent<Image>().color = selected;
    //                //ModeButtons[i].GetComponentInChildren<Text>().color = notselected;
    //            }
    //            else {
    //                selectedModeButton = ModeButtons[i];

    //            }

    //        }
    //    }

    //    private void moveModeButtons() {

    //        foreach (GameObject b in ModeButtons)
    //        {

    //            if (b != selectedModeButton)
    //            {
    //                float scaleFactor = (b.transform.position.x - buttonrestcentre) * (b.transform.position.x - buttonrestcentre);
    //                if ((b.transform.position.x - buttonrestcentre) > 0.9f && dir0 == -1)
    //                {
    //                    dir0 = +1;
    //                }
    //                else if((b.transform.position.x - buttonrestcentre)<-0.9f && dir0 == +1) {
    //                    dir0 = -1;
    //                }


    //                   b.transform.position -= dir0 * buttonmovespeed0 * Vector3.right * Time.deltaTime* scaleFactor;

    //            }
    //            else
    //            {
    //                float scaleFactor = (b.transform.position.x - buttonselectcentre) * (b.transform.position.x - buttonselectcentre);
    //                if ((b.transform.position.x - buttonselectcentre) > 0.9f && dir1 == -1)
    //                {
    //                    dir1 = +1;
    //                }
    //                else if ((b.transform.position.x - buttonselectcentre) < -0.9f && dir1 == +1)
    //                {
    //                    dir1 = -1;
    //                }


    //                b.transform.position -= dir1 * buttonmovespeed0 * Vector3.right * Time.deltaTime* scaleFactor;
    //            }

    //        }
    //    }

    //    public void modeChange(GameObject thisbutton) {
    //        if (thisbutton != ModeButtons[2])
    //        {
    //            greyOutTheAI();
    //            levelconfig.isAIlevel = false;
    //        }
    //        else {
    //            levelconfig.isAIlevel = true;
    //            selectthisbutton(selectedAIButton);
    //        }
    //    }

    //    private void greyOutTheAI() {
    //        foreach (GameObject b in AIButtons)
    //        {
    //            if (b != selectedAIButton)
    //            {
    //                b.GetComponent<Button>().GetComponent<Image>().color = greyedout;
    //                b.GetComponentInChildren<Text>().color = greydouttext;

    //            }
    //            else
    //            {
    //                b.GetComponent<Button>().GetComponent<Image>().color = chosengreyedout;
    //                b.GetComponentInChildren<Text>().color = chosengreydouttext;

    //            }
    //        }


    //    }

    //    //public void load2pGravity()
    //    //{
    //    //    LoadingScreenManager.LoadScene(2);
    //    //}

    //    //public void loadMMnoGravity()
    //    //{
    //    //    LoadingScreenManager.LoadScene(4);
    //    //}

    //    //public void loadMMGravity()
    //    //{
    //    //    LoadingScreenManager.LoadScene(5);
    //    //}
    //    //public void loadAI()
    //    //{
    //    //    LoadingScreenManager.LoadScene(6);
    //    //}

    void disableAIs()
    {
        for (int i=0; i< AIButtons.Length; i++)
        {
            if (i>AIlevel)
            {
                AIButtons[i].GetComponent<Button>().GetComponent<Image>().color = greyedout;
                AIButtons[i].GetComponentInChildren<Text>().color = greydouttext;
                //AIButtons[i].GetComponent<Button>().interactable = false;
            }
            
        }
    }

    // choose
    public void choseAI(GameObject b) {

        for (int i = 0; i < AIButtons.Length; i++)
        {
            if (b == AIButtons[i] )
            {
                levelconfig.setAIConfig = (AItype)i;
            }

        }

        // may need this for animation of AI graphics
        //if (b == AIButtons[0]) {

        //} else if (b == AIButtons[1]) {
        //}
        //else if (b == AIButtons[2]) {
        //}
        //else if (b == AIButtons[3]) {
        //}
        //else if (b == AIButtons[4]) {
        //}
        //else if (b == AIButtons[5]) { }
        //else if (b == AIButtons[6]) { }
        //else if (b == AIButtons[7]) { }
        

    }

}



