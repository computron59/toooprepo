﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LPlayerScoreScript : MonoBehaviour {

    public AudioClip boom;
    private AudioSource source;
    bool canscore = true;

    //public GameObject CrashNBurnModelPrefab;
    private GameObject Rplayer;
    private HUD_Behavour_Script HUDscript;
    touchReceiver tr;
    Right_Shield_Controller RSheild_Controller;

    void Start()
    {
        RSheild_Controller = FindObjectOfType<Right_Shield_Controller>();
        tr = FindObjectOfType<touchReceiver>();
        source = GetComponent<AudioSource>();
        HUDscript = GameObject.FindGameObjectWithTag("HUD").GetComponent<HUD_Behavour_Script>();

        Rplayer = GameObject.FindGameObjectWithTag("R_Player_Container");
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "ball" && this.GetComponent<goal_behaviour_generic>().goalActive && canscore)
        {
            canscore = false;
            Destroy(other.gameObject);
            HUDscript.OpponentHearts -= 1;
            Rplayer.SetActive(false);
            Vector3 CNBstartPos = Rplayer.transform.position;
            Instantiate(Resources.Load("RCrashNBurn"), CNBstartPos, Quaternion.identity);
            StartCoroutine(RplayerReset(2.5f));
            StartCoroutine(playBoomAtRightTime(1.6f));
        }
    }

    IEnumerator RplayerReset(float delay)
    {
        yield return new WaitForSeconds(delay);
        tr.LPlayerTouchPop = false;
        tr.Rpoptime = 0;
        tr.RpopSet = false;
        Rplayer.SetActive(true);
        canscore = true;
        RSheild_Controller.ResetSheild();

    }

    IEnumerator playBoomAtRightTime(float delay)
    {
        yield return new WaitForSeconds(delay);
        source.volume = 1.5f;
        source.PlayOneShot(boom);

    }
}
