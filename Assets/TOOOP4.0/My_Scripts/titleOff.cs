﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class titleOff : MonoBehaviour {

	// Use this for initialization
	void Start () {
        if (!FindObjectOfType<Right_PlayerAI>().isActiveAndEnabled) {
            this.gameObject.SetActive(false);
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
