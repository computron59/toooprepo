﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class repulsorScript : MonoBehaviour {

    GameObject[] balls;
    

    public bool isrepulsor= true; 

    public enum gravModes { normalSqRule, linear, cubeRule, customRule }
    public gravModes thismode;
    public float customGravFactor = 2.6f / 3f;

    public long sqrepulsion = 10000000;
    public long linrepulsion = 1000;
    public long cuberepulsion = 1000000000000;
    public long customrepulsion = 1000000000000;

    int forceDir = 1;

    public float maxDistance = 250f;

	// Use this for initialization
	void Start () {
        if (!isrepulsor) {
            isrepulsor = true;
            forceDir *= -1;
        }
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        balls = GameObject.FindGameObjectsWithTag("ball");

        switch (thismode) {

            

            case gravModes.normalSqRule:
                for (int i = 0; i < balls.Length; i++)
                {
                    float dist = Vector3.Distance(transform.position, balls[i].transform.position);
                    if (dist> maxDistance) {
                        return;
                    }
                    Vector3 vec = Vector3.Normalize(balls[i].transform.position - transform.position);
                    balls[i].gameObject.GetComponent<Rigidbody>().AddForce(sqrepulsion * Time.deltaTime * vec * forceDir / (dist * dist));
                }
                break;
            case gravModes.linear:
                for (int i = 0; i < balls.Length; i++)
                {
                    float dist = Vector3.Distance(transform.position, balls[i].transform.position);
                    if (dist > maxDistance)
                    {
                        return;
                    }
                    Vector3 vec = Vector3.Normalize(balls[i].transform.position - transform.position);
                    balls[i].gameObject.GetComponent<Rigidbody>().AddForce(linrepulsion * Time.deltaTime * vec * forceDir / (dist));
                }
                break;
            case gravModes.cubeRule:
                for (int i = 0; i < balls.Length; i++)
                {
                    float dist = Vector3.Distance(transform.position, balls[i].transform.position);
                    if (dist > maxDistance)
                    {
                        return;
                    }
                    Vector3 vec = Vector3.Normalize(balls[i].transform.position - transform.position);
                    balls[i].gameObject.GetComponent<Rigidbody>().AddForce(cuberepulsion * Time.deltaTime * vec * forceDir / (dist * dist * dist));
                }
                break;
            case gravModes.customRule:
                for (int i = 0; i < balls.Length; i++)
                {
                    float dist = Vector3.Distance(transform.position, balls[i].transform.position);
                    if (dist > maxDistance)
                    {
                        return;
                    }
                    Vector3 vec = Vector3.Normalize(balls[i].transform.position - transform.position);
                    balls[i].gameObject.GetComponent<Rigidbody>().AddForce(customrepulsion * Time.deltaTime * vec * forceDir / ( Mathf.Pow(dist, customGravFactor)));
                }
                break;
        }



        if (thismode == gravModes.normalSqRule) {
            


        }
	}
}
