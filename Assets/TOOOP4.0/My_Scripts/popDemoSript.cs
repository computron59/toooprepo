﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class popDemoSript : MonoBehaviour {


    public GameObject thumbSmall;
    public GameObject thumbLarger;
    public GameObject smallShip;
    public GameObject bigShip;

    float timer;
    public float flashtime = 1.2f;

	// Use this for initialization
	void Start () {
        thumbLarger.SetActive(false);
        bigShip.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;


        if (timer >= flashtime) {
            
            timer = 0;
            
            if (thumbSmall.activeSelf == true)
            {
                thumbSmall.SetActive(false);
                thumbLarger.SetActive(true);

                smallShip.SetActive(false);
                bigShip.SetActive(true);
            }

            else  {              
                thumbLarger.SetActive(false);
                thumbSmall.SetActive(true);

                smallShip.SetActive(true);
                bigShip.SetActive(false);
            }
        }
	}
}
