﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RPlayerScoreScript : MonoBehaviour {

    private AudioSource source;
    public AudioClip boom;
    bool canscore = true;

    //private GameObject CrashNBurnModelPrefab;
    private GameObject Lplayer;
    private HUD_Behavour_Script HUDscript;
    touchReceiver tr;
    Shield_Controller LSheildController;

    void Start()
    {
        LSheildController = FindObjectOfType<Shield_Controller>();
        tr = FindObjectOfType<touchReceiver>();
        source = GetComponent<AudioSource>();
        HUDscript = GameObject.FindGameObjectWithTag("HUD").GetComponent<HUD_Behavour_Script>();
        Lplayer = GameObject.FindGameObjectWithTag("L_Player_Container");
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "ball" && this.GetComponent<goal_behaviour_generic>().goalActive && canscore)
        {
            canscore = false;
            Destroy(other.gameObject);
            HUDscript.Hearts -= 1;
            Lplayer.SetActive(false);
            Vector3 CNBstartPos = Lplayer.transform.position;
            Instantiate(Resources.Load("LCrashNBurn"), CNBstartPos, Quaternion.identity);
            StartCoroutine(LplayerReset(2.5f));
            StartCoroutine(playBoomAtRightTime(1.6f));

        }
    }

    IEnumerator LplayerReset(float delay) {
        yield return new WaitForSeconds(delay);
        tr.LPlayerTouchPop = false;
        tr.Lpoptime = 0;
        tr.LpopSet = false;
        Lplayer.SetActive(true);
        canscore = true;
        LSheildController.resetSheild();
    }

    IEnumerator playBoomAtRightTime(float delay)
    {
        yield return new WaitForSeconds(delay);
        source.volume = 1.5f;
        source.PlayOneShot(boom);

    }
}
