﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class arrowMove : MonoBehaviour {

    float dir = +1;
    float startpos;
	// Use this for initialization
	void Start () {
        startpos = transform.position.y;
	}
	
	// Update is called once per frame
	void Update () {

        float scaleFactor = 75;
        
        float limit0 = 150f + startpos ;
        float limit1 = -150f + startpos;


        this.transform.position += Vector3.up * scaleFactor * dir * Time.deltaTime;


        if (this.transform.position.y >= limit0 && dir == 1) {
            dir = -1;
            this.transform.position = new Vector3(this.transform.position.x, limit0, this.transform.position.z);
        }

        if (this.transform.position.y <= limit1 && dir == -1)
        {
            dir = +1;
            this.transform.position = new Vector3(this.transform.position.x, limit1, this.transform.position.z);
        }


        
	}
}
