﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class portalScript : MonoBehaviour {

    //public bool gravityPortal = true;
    bool readytogo = true;
    GameObject[] portals;
    Stack<GameObject> otherPortals;
    GameObject[] otherPortalsArray;

	// Use this for initialization
	void Start () {

        otherPortals = new Stack<GameObject>();

        portals = GameObject.FindGameObjectsWithTag("portal");
        foreach(GameObject p in portals)
        {
            if (p != this.gameObject ) {
                otherPortals.Push(p);
            }
        }
        otherPortalsArray = otherPortals.ToArray();
        
	}

    private void OnTriggerEnter(Collider other)
    {


        if (other.transform.tag=="ball" && readytogo) {
            int exit = otherPortals.Count;
            int randexit = Random.Range(0,(exit));


            other.transform.position = otherPortalsArray[randexit].transform.position;
            readytogo = true;
            otherPortalsArray[randexit].GetComponent<portalScript>().readytogo = false;
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.transform.tag == "ball") {
            this.readytogo = true;

        }
    }


}
