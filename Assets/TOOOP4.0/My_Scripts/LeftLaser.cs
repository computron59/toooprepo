﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// the laser script
public class LeftLaser : MonoBehaviour {


    private level_Config configObj;



    //a public variable to tinker with the impulse
    private float laserForce = 18000;
    public float laser_flapForce = 2000;
    public long laser_TriangleForce = 60000000000;
    public long laser_FlopForce = 1500000000;

    //audio
    private AudioSource source;

    public AudioClip laser;

    private LineRenderer LR;
    private GameObject hitobject;
    private GameObject left_hit_effect;
    private GameObject muzzleFX;

    private int sideofcentreobj = 1;

    //private GameObject ball;

    // the bool from the shield script
    private bool laseronPrivate;
    public bool laseron
    {
        get
        {
            return laseronPrivate;
        }
        set
        {
            laseronPrivate = value;
        }
    }

    

    private void Start()
    {
        configObj = FindObjectOfType<level_Config>();

        source = GetComponent<AudioSource>();

        //ball = GameObject.FindGameObjectWithTag("ball");
        muzzleFX = GameObject.FindGameObjectWithTag("L_Muzzle");
        muzzleFX.SetActive(false);
        hitobject = GameObject.FindGameObjectWithTag("L_LaserHitObj");
        left_hit_effect = GameObject.FindGameObjectWithTag("LeftHitEffect");
        left_hit_effect.SetActive(false);
        LR = GetComponent<LineRenderer>();
        LR.enabled = false;
    }

    void Update () {

        if (transform.position.y < 0) {
            sideofcentreobj = -1;
        }
        else
        {
            sideofcentreobj = 1;
        }


        //some noise on the linerenderer
        //LR.startWidth=(Mathf.PerlinNoise(Time.time,0)*5)+3;


        //the lasers raycast
        RaycastHit LeftLaserHit;

        if (!configObj.autoLasers || GameObject.FindGameObjectWithTag("ball")== false)
        {

            if (Physics.Raycast(transform.position, Vector3.right, out LeftLaserHit, 15000))
            {
                hitobject.transform.position = new Vector3(LeftLaserHit.point.x,
                transform.position.y, transform.position.z);

                //the laser-ball impulse
                if (LeftLaserHit.transform.gameObject.tag == "ball" && laseronPrivate)
                {
                    LeftLaserHit.transform.gameObject.GetComponent<Rigidbody>().AddForce(laserForce * Vector3.right * Time.deltaTime);

                    // the ABANDONED sonic interaction
                }
                else if ((LeftLaserHit.transform.gameObject.tag == "Lsonic00" || LeftLaserHit.transform.gameObject.tag == "Rsonic00"
                   || LeftLaserHit.transform.gameObject.tag == "LMegasonic" || LeftLaserHit.transform.gameObject.tag == "RMegasonic") && laseronPrivate)
                {
                    LeftLaserHit.transform.gameObject.GetComponent<sonic_generic_script>().popsonic = true;
                }

                //the laser-flap inpulse
                else if (LeftLaserHit.transform.gameObject.tag == "flap" && laseronPrivate)
                {
                    LeftLaserHit.transform.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(LeftLaserHit.transform.position, laser_flapForce * Vector3.right);
                }

                //the laser-triangle inpulse
                else if (LeftLaserHit.transform.gameObject.tag == "rotating_tri" && laseronPrivate)
                {

                    LeftLaserHit.transform.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(LeftLaserHit.transform.position, laser_TriangleForce * Vector3.right * sideofcentreobj);
                }

                //the laser-flop inpulse
                else if (LeftLaserHit.transform.gameObject.tag == "flop" && laseronPrivate)
                {
                    Debug.Log("hit the flop");
                    Vector3 relpt = (LeftLaserHit.transform.position);
                    LeftLaserHit.transform.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(relpt, laser_FlopForce * Vector3.right * sideofcentreobj);
                }

            }
            else
            {
                hitobject.transform.position = new Vector3(transform.position.x + 1500,
                transform.position.y, transform.position.z);
            }

        } else if(GameObject.FindGameObjectWithTag("ball")!=false && configObj.autoLasers)
        {
            GameObject ballObj = GameObject.FindGameObjectWithTag("ball");
            Vector3 dir = (ballObj.transform.position - transform.position).normalized;
            if (Physics.Raycast(transform.position, dir, out LeftLaserHit, 15000) && laseronPrivate) {
                hitobject.transform.position = LeftLaserHit.point;
                if (LeftLaserHit.transform.tag == "ball") {

                    ballObj.GetComponent<Rigidbody>().AddForce(laserForce * dir * Time.deltaTime);
                }

                ///////////////////laser interactions
                //the laser-flap inpulse
                else if (LeftLaserHit.transform.gameObject.tag == "flap" && laseronPrivate)
                {
                    LeftLaserHit.transform.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(LeftLaserHit.transform.position, laser_flapForce * dir);
                }

                //the laser-triangle inpulse
                else if (LeftLaserHit.transform.gameObject.tag == "rotating_tri" && laseronPrivate)
                {

                    LeftLaserHit.transform.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(LeftLaserHit.transform.position, laser_TriangleForce * dir * sideofcentreobj);
                }

                //the laser-flop inpulse
                else if (LeftLaserHit.transform.gameObject.tag == "flop" && laseronPrivate)
                {
                    Debug.Log("hit the flop");
                    Vector3 relpt = (LeftLaserHit.transform.position);
                    LeftLaserHit.transform.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(relpt, laser_FlopForce * dir * sideofcentreobj);
                }
                //////////////////////////////////////



            }


        }

        LR.SetPosition(0, new Vector3(transform.position.x+10, transform.position.y, transform.position.z));
        LR.SetPosition(1, hitobject.transform.position);

        if (laseronPrivate)
        {
            source.volume = 0.01f;
            source.PlayOneShot(laser);
            hitobject.GetComponent<ParticleSystem>().Play();
            left_hit_effect.SetActive(true);
            muzzleFX.SetActive(true);
            LR.enabled = true; 
        }
        else
        {
            if (hitobject.GetComponent<ParticleSystem>().isPlaying) {
                hitobject.GetComponent<ParticleSystem>().Stop();
            }

            source.Stop();
            
            muzzleFX.SetActive(false);
            LR.enabled = false;
            left_hit_effect.SetActive(false);
        }
	}
}
