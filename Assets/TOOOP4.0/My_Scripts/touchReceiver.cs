﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.EventSystems;

public class touchReceiver : MonoBehaviour
{
    public bool istouch = true; // bool used elsewhere

    //button stuff
    GameObject myEventSystem;
    


    public float LPlayerY = 0;
    public float RPlayerY = 0;
    public bool LPlayerTouchPop = false;
    public bool RPlayerTouchPop = false;
    public bool LPlayerLaser = false;
    public bool RPlayerLaser = false;

    public float Lpoptime = 0;
    public float popTimeSet = 0.4f;
    public bool LpopSet = false;

    public float Rpoptime = 0;
    public bool RpopSet = false;

    Camera C;
    Dictionary<int, float> Ltouches;    // dictionary of fingerid and time
    Dictionary<int, float> Rtouches;

    Touch RselectedTouch;
    Touch selectedTouch;    // touch selected.
    float latestTime=1000000;   //11 days
    float RlatestTime = 1000000;
    //int LtouchCount=0;

    bool AIisActive = false;

    private void Start()
    {
        myEventSystem = GameObject.Find("EventSystem");
        Ltouches = new Dictionary<int, float>();
        Rtouches = new Dictionary<int, float>();
        C = FindObjectOfType<Camera>();
        AIisActive = (FindObjectOfType<Right_PlayerAI>().isActiveAndEnabled) ? true : false;
    }

    // consider the touch moves over the midpoint!!!!
    private void Update()   
    {
        latestTime = 1000000;
        RlatestTime = 1000000;


        // dictionary of active touches
        for (int i = 0; i < Input.touchCount; i++)
        {
            bool isbuttonPress = EventSystem.current.IsPointerOverGameObject(Input.GetTouch(i).fingerId);



            if (Input.GetTouch(i).position.x < (Screen.width / 2) && Input.GetTouch(i).phase == TouchPhase.Began && !isbuttonPress) {

                float currenttime = Time.time;
                if (!Ltouches.ContainsValue(currenttime) && Input.GetTouch(i).position.x < Screen.width / 2 ) {
                    Ltouches.Add(Input.GetTouch(i).fingerId, currenttime);
                }
            }
            if ((Input.GetTouch(i).position.x > Screen.width / 2) || Input.GetTouch(i).phase == TouchPhase.Ended || isbuttonPress)
            {
                Ltouches.Remove(Input.GetTouch(i).fingerId);
            }

            if (!AIisActive) {
                if (Input.GetTouch(i).position.x > (Screen.width / 2) && Input.GetTouch(i).phase == TouchPhase.Began && !isbuttonPress)
                {

                    float currenttime = Time.time;
                    if (!Rtouches.ContainsValue(currenttime) && Input.GetTouch(i).position.x > Screen.width / 2)
                    {
                        Rtouches.Add(Input.GetTouch(i).fingerId, currenttime);
                    }
                }
                if ((Input.GetTouch(i).position.x < Screen.width / 2) || Input.GetTouch(i).phase == TouchPhase.Ended || isbuttonPress)
                {
                    Rtouches.Remove(Input.GetTouch(i).fingerId);
                }
            }
        }

        //gets the oldest touch in dictionary. need only be done when dictionary size changes..... unless finger replaced in one frame?
        float selectedkey=0;
        if ( Ltouches.Count >= 1 /*&& LtouchCount != Ltouches.Count*/)
        {
            //LtouchCount = Ltouches.Count;
            foreach (KeyValuePair<int, float> i in Ltouches)
            {  // for active touches

                // if time is earliest yet
                if (i.Value < latestTime)
                {
                    latestTime = i.Value;
                    selectedkey = i.Key;
                }
            }

            // selects relevant touch 
            for (int j = 0; j < Input.touchCount; j++)
            {
                if (Input.GetTouch(j).fingerId == selectedkey)
                {
                    selectedTouch = Input.GetTouch(j);

                }
            }
        }

        //same for the right
        float Rselectedkey = 0;
        if (Rtouches.Count >= 1 /*&& LtouchCount != Ltouches.Count*/)
        {
            //LtouchCount = Ltouches.Count;
            foreach (KeyValuePair<int, float> i in Rtouches)
            {  // for active touches

                // if time is earliest yet
                if (i.Value < RlatestTime)
                {
                    RlatestTime = i.Value;
                    Rselectedkey = i.Key;
                }
            }

            // selects relevant touch 
            for (int j = 0; j < Input.touchCount; j++)
            {
                if (Input.GetTouch(j).fingerId == Rselectedkey)
                {
                    RselectedTouch = Input.GetTouch(j);

                }
            }
        }



        if (Ltouches.Count >= 1)
        {
            Lpoptime += Time.deltaTime;     // if 0.4s expired setup for pop.
            if (Lpoptime >= popTimeSet)
            {
                LpopSet = true;
            }
            LPlayerY = C.ScreenToWorldPoint(selectedTouch.position).y;
        }

        //if (Ltouches.Count > 1)   // Laser simples
        //{
        //    LPlayerLaser = true;
        //}
        //else
        //{
        //    LPlayerLaser = false;
        //}

        // Left Pop
        if (Ltouches.Count == 0 && LpopSet)
        {
            LpopSet = false;
            Lpoptime = 0;
            LPlayerTouchPop = true;
        }

        if (Ltouches.Count == 0 && !LpopSet)
        {
            Lpoptime = 0;
        }

        //R
        if (Rtouches.Count >= 1)
        {
            Rpoptime += Time.deltaTime;     // if 0.4s expired setup for pop.
            if (Rpoptime >= popTimeSet)
            {
               RpopSet = true;
            }
            RPlayerY = C.ScreenToWorldPoint(RselectedTouch.position).y;
        }

        //if (Rtouches.Count > 1)   // Laser simples
        //{
        //    RPlayerLaser = true;
        //}
        //else
        //{
        //    RPlayerLaser = false;
        //}

        // Left Pop
        if (Rtouches.Count == 0 && RpopSet)
        {
            RpopSet = false;
            Rpoptime = 0;
            RPlayerTouchPop = true;
        }

        if (Rtouches.Count == 0 && !RpopSet)
        {
            Rpoptime = 0;
        }

    }
}
//public class touchReceiver : MonoBehaviour {

//    bool isonrepeat;

//    //bool repeatL = false;
//    //bool repeatR = false;

//    Camera C;
//    public bool istouch = false;
//    public float LPlayerY = 0;
//    public float RPlayerY = 0;
//    public bool LPlayerTouchPop = false;
//    public bool RPlayerTouchPop = false;
//    public bool LPlayerLaser = false;
//    public bool RPlayerLaser = false;

//    private bool Lpopset = false;
//    private bool Rpopset = false;

//    private float popTimeSet=0.42f;

//    private float Lpoptime = 0;
//    private float Rpoptime = 0;


//    int numLtouches = 0;
//    int numRtouches = 0;

//    Touch[] Ltouches;
//    Touch[] Rtouches;

//    // Use this for initialization
//    void Start () {
//        C = FindObjectOfType<Camera>();

//    }

//	// Update is called once per frame
//	void Update () {

//        Ltouches = new Touch[100];  // I doubt 100 fingers can fit on the screen.
//        Rtouches = new Touch[100];  // I doubt 100 fingers can fit on the screen.

//        float[] Lstarttimes = new float[100];
//        float[] Rstarttimes = new float[100];

//        int savedLfirstTouch = 0;
//        int savedRfirstTouch = 0;


//        isonrepeat = false;


//        numLtouches = 0;
//        numRtouches = 0;


//        // touch counter

//        foreach (Touch touch in Input.touches) {


//            //L touch array
//            if ((touch.position.x < Screen.width / 2) && (touch.phase != TouchPhase.Ended && touch.phase != TouchPhase.Canceled) 
//                && (touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary ))
//            {
//                Ltouches[numLtouches] = touch;
//                //if (touch.phase == TouchPhase.Began) {
//                //    Lstarttimes[numLtouches] = Time.time;
//                //}
//                numLtouches++;
//            }

//            //L touch array
//            if ((touch.position.x < Screen.width / 2) && (touch.phase != TouchPhase.Ended && touch.phase != TouchPhase.Canceled) 
//                && touch.phase == TouchPhase.Began)
//            {
//                Ltouches[numLtouches] = touch;
//                //if (touch.phase == TouchPhase.Began) {
//                //    Lstarttimes[numLtouches] = Time.time;
//                //}
//                numLtouches++;
//            }




//            if ((touch.position.x < Screen.width / 2) && touch.phase == TouchPhase.Ended && !isonrepeat)
//            {
//                numLtouches = 0;
//                foreach (Touch retryTouch in Input.touches)
//                {


//                    //L touch array
//                    if ((retryTouch.position.x < Screen.width / 2) && (retryTouch.phase != TouchPhase.Ended && retryTouch.phase != TouchPhase.Canceled)
//                        && (retryTouch.phase == TouchPhase.Moved || retryTouch.phase == TouchPhase.Stationary))
//                    {
//                        Ltouches[numLtouches] = retryTouch;
//                        //if (touch.phase == TouchPhase.Began) {
//                        //    Lstarttimes[numLtouches] = Time.time;
//                        //}
//                        numLtouches++;
//                    }

//                    //L touch array
//                    if ((retryTouch.position.x < Screen.width / 2) && (retryTouch.phase != TouchPhase.Ended && retryTouch.phase != TouchPhase.Canceled)
//                        && retryTouch.phase == TouchPhase.Began)
//                    {
//                        Ltouches[numLtouches] = retryTouch;
//                        //if (touch.phase == TouchPhase.Began) {
//                        //    Lstarttimes[numLtouches] = Time.time;
//                        //}
//                        numLtouches++;
//                    }

//                }
//            }




//            // R touch array
//            // R touch array
//            if ((touch.position.x > Screen.width / 2) && (touch.phase != TouchPhase.Ended && touch.phase != TouchPhase.Canceled))
//            {
//                Rtouches[numRtouches] = touch;
//                numRtouches++;
//            }
//        }


//        //if (numLtouches==1) {

//        //    Ltouches[0] = 
//        //}
//        ////  End of touch counter
//        //float diff = 1000000;   // one million seconds - 11 days.

//        //// should select earliest touch!!!
//        //for (int i=0; i<numLtouches;i++) {
//        //    if (Lstarttimes[i] < diff) {
//        //        diff = Lstarttimes[i];
//        //        savedLfirstTouch = i;
//        //    }
//        //}



//        //RIGHT

//        // more than or one touch, movement, add to popcounter.
//        if (numRtouches >= 1)
//        {
//            RPlayerY = C.ScreenToWorldPoint(Rtouches[0].position).y;    // Right Movement is first touch position.

//            Rpoptime += Time.deltaTime;     // if 0.4s expired setup for pop.
//            if (Rpoptime >= popTimeSet)
//            {
//                Rpopset = true;
//            }
//        }

//        if (numRtouches >= 2)   // Laser simples
//        {
//            RPlayerLaser = true;
//        }

//        if(numRtouches < 2)
//        {
//            RPlayerLaser = false;
//        }


//        // Right Pop
//        if (numRtouches == 0 && Rpopset)
//        {
//            Rpopset = false;
//            RPlayerTouchPop = true;
//        }

//        if (numRtouches == 0 && !Rpopset)
//        {
//            Rpoptime = 0;
//        }

//        //LEFT

//        // one touch Left side
//        if (numLtouches >= 1)
//        {
//            LPlayerY = C.ScreenToWorldPoint(Ltouches[0].position).y;    // Left movement

//            Lpoptime += Time.deltaTime; // set pop if time gathered is enough.
//            if (Lpoptime >= popTimeSet)
//            {
//                Lpopset = true;
//            }
//        }

//        if (numLtouches >= 2)   // Laser, simples.
//        {
//            LPlayerLaser = true;
//        }

//        if(numLtouches < 2)
//        {
//            LPlayerLaser = false;
//        }

//        //  successful pop
//        if (numLtouches == 0 && Lpopset)
//        {
//            Lpopset = false;
//            Lpoptime = 0;
//            LPlayerTouchPop = true;
//        }

//        // unsuccessful pop
//        if (numLtouches == 0 && !Lpopset)
//        {
//            Lpoptime = 0;
//        }
//    }


//    //L laser Old Method
//    //if (touchinQuestion.position.x < Screen.width / 2) {
//    //    if (touchinQuestion.position.x > Screen.width * 0.1724f && (touchinQuestion.phase == TouchPhase.Moved || touchinQuestion.phase == TouchPhase.Stationary))
//    //    {
//    //        LPlayerLaser = true;
//    //    }

//    //    else
//    //    {
//    //        LPlayerLaser = false;
//    //    }
//    //}

//    // Rlaser Old method
//    //if (touchinQuestion.position.x >  Screen.width /2) {
//    //    if (touchinQuestion.position.x < Screen.width * 0.8276f && (touchinQuestion.phase == TouchPhase.Moved || touchinQuestion.phase == TouchPhase.Stationary))
//    //    {
//    //        RPlayerLaser = true;
//    //    }
//    //    else
//    //    {
//    //        RPlayerLaser = false;
//    //    }

//    //}


//    //// R pop
//    //if (touchinQuestion.position.x > 2*Screen.width / 3)
//    //{
//    //    RPlayerY = C.ScreenToWorldPoint(touchinQuestion.position).y;
//    //    if (touchinQuestion.phase == TouchPhase.Ended)
//    //    {
//    //        RPlayerLaser = false;
//    //        RPlayerTouchPop = true;
//    //    }



//    //}

//}
