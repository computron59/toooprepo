﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public enum AItype { Reuben=0, Balthazar=1, Rupert=2, Edwald=3, Barnaby=4, Bentham=5, Clayton=6, Addison=7 }


public class Right_PlayerAI : MonoBehaviour
{
    public AItype thisAI;
    private Right_Shield_Controller RSC;
    private GameObject Rshield;
    private GameObject humanPlayer;
    RightLaser RL;
    Vector3 ball;
    int direction = -1;
    float scaletodistance = 2.3f;
    bool backToCentre = false;
    bool variableIdlespeedInitialised = false;
    float variableTrackSpeed;
    bool isBeingTracked=false;
    bool lostInterest = false;
    float currtrackdistance;
    float timer = 0;
    float playerLimit=185f;
    bool stopped=false;
    bool variabletrackspeedinitialised = false;
    bool trackspeedSet = false;
    float speed;

    public Text name = null;

    HUD_Behavour_Script HBS;


    // AI vairables
    float maxtrackdistance;
    float mintrackdistance;
    bool possibleToloseInterest=false;
    int chanceOfLosinginterest;
    float trackspeed;
    //bool periodicZap = false;
    //float lasertimeron;
    //float lasertimeoff;
    bool stopstracking =false; 
    int chanceoftotalstop;
    float stoptimer=0; 
    float stoptime;
    bool dumbpops = false;
    float dumbpopradius;
    float dumbinnerpopradius;
    float dumbpopNRG;

    bool badlaser = false;
    float badlaserAngle;


    int multipleZap = 0;
    int zappsDone = 0;
    float zapstartTime = 0.5f;
    float zapendTime = 0.8f;
    float dblZapstarttime = 1.2f;
    float dblZapendtime = 1.5f;
    float tplZapstarttime = 1.7f;
    float tplZapendtime = 1.9f;

    bool pauseWhileLaser = false;
    bool laserPause = false;

    float MAXtrack;
    float MINtrack;

    float MINidle;
    float MAXidle;




    private void Start()
    {
        thisAI = FindObjectOfType<level_Config>().setAIConfig;
        HBS = FindObjectOfType<HUD_Behavour_Script>();
        Rshield = GameObject.FindGameObjectWithTag("Rshield");
        RSC = Rshield.GetComponent<Right_Shield_Controller>();
        humanPlayer = GameObject.FindGameObjectWithTag("L_Player_Container");
        RL = GetComponent<RightLaser>();
        

        if (thisAI== AItype.Reuben) {

            name.text = "Reuben"; 
            maxtrackdistance = 260f;
            mintrackdistance = 200f;            
            trackspeed = 45f;

            possibleToloseInterest = false;
            chanceOfLosinginterest = 120;

            //periodicZap = false;
            //lasertimeron = .60f;
            //lasertimeoff = 3.2f;

            stopstracking = true;
            chanceoftotalstop = 180;
            stoptime = 3f;

            dumbpops = true;// this deliberately pops at the wrong time.
            dumbpopradius = 220f;
            dumbinnerpopradius = 160f;
            dumbpopNRG = 80f;

            badlaser = false;
            badlaserAngle = 35f;

            multipleZap = 3;    // up to 4 means 3 zaps
            zapstartTime = 2.6f;
            zapendTime = 2.8f;
            dblZapstarttime = 3.0f;
            dblZapendtime = 3.4f;
            tplZapstarttime = 3.6f;
            tplZapendtime = 4.0f;

            pauseWhileLaser = true;

            MINtrack = 40f;
            MAXtrack = 80f;

            MINidle = 45;
            MAXidle = 120;
        }
        else if (thisAI == AItype.Balthazar)
        {

            name.text = "Balthazar";
            maxtrackdistance = 260f;
            mintrackdistance = 200f;
            trackspeed = 45f;

            possibleToloseInterest = false;
            chanceOfLosinginterest = 120;

            //periodicZap = false;
            //lasertimeron = .60f;
            //lasertimeoff = 3.2f;

            stopstracking = false;
            chanceoftotalstop = 180;
            stoptime = 3f;

            dumbpops = true;// this deliberately pops at the wrong time.
            dumbpopradius = 220f;
            dumbinnerpopradius = 160f;
            dumbpopNRG = 80f;

            badlaser = false;
            badlaserAngle = 35f;

            multipleZap = 4;    // up to 3 means 3 zaps
            zapstartTime = 2.6f;
            zapendTime = 2.8f;
            dblZapstarttime = 3.0f;
            dblZapendtime = 3.8f;
            tplZapstarttime = 4.0f;
            tplZapendtime = 4.1f;

            pauseWhileLaser = true;

            MINtrack = 40f;
            MAXtrack = 100f;

            MINidle = 90;
            MAXidle = 180;
        }
        else if (thisAI == AItype.Rupert)
        {

            name.text = "Rupert";
            maxtrackdistance = 380f;
            mintrackdistance = 180f;
            trackspeed = 45f;

            possibleToloseInterest = true;
            chanceOfLosinginterest = 800;

            //periodicZap = false;
            //lasertimeron = .60f;
            //lasertimeoff = 3.2f;

            stopstracking = false;
            chanceoftotalstop = 20;
            stoptime = 3f;

            dumbpops = false;// this deliberately pops at the wrong time.
            dumbpopradius = 220f;
            dumbinnerpopradius = 160f;
            dumbpopNRG = 80f;

            badlaser = true;
            badlaserAngle = 35f;

            multipleZap = 0;
            zapstartTime = 2.6f;
            zapendTime = 3.2f;
            dblZapstarttime = 3.6f;
            dblZapendtime = 4.2f;
            tplZapstarttime = 4.5f;
            tplZapendtime = 4.6f;

            pauseWhileLaser = false;

            MINtrack = 15f;
            MAXtrack = 25f;

            MINidle = 45;
            MAXidle = 120;
        }
        else if (thisAI == AItype.Edwald)
        {

            name.text = "Edwald";
            maxtrackdistance = 540f;
            mintrackdistance = 120f;
            trackspeed = 45f;

            possibleToloseInterest = true;
            chanceOfLosinginterest = 800;

            //periodicZap = false;
            //lasertimeron = .60f;
            //lasertimeoff = 3.2f;

            stopstracking = false;
            chanceoftotalstop = 20;
            stoptime = 3f;

            dumbpops = true;// this deliberately pops at the wrong time.
            dumbpopradius = 180f;
            dumbinnerpopradius = 140f;
            dumbpopNRG = 80f;

            badlaser = false;
            badlaserAngle = 35f;

            multipleZap = 2;
            zapstartTime = 2.6f;
            zapendTime = 3.2f;
            dblZapstarttime = 3.6f;
            dblZapendtime = 4.2f;
            tplZapstarttime = 4.5f;
            tplZapendtime = 4.6f;

            pauseWhileLaser = true;

            MINtrack = 10f;
            MAXtrack = 30f;

            MINidle = 45;
            MAXidle = 120;
        }
        else if (thisAI == AItype.Barnaby)
        {

            name.text = "Barnaby";
            maxtrackdistance = 380f;
            mintrackdistance = 200f;
            trackspeed = 45f;

            possibleToloseInterest = false;
            chanceOfLosinginterest = 800;

            //periodicZap = false;
            //lasertimeron = .60f;
            //lasertimeoff = 3.2f;

            stopstracking = false;
            chanceoftotalstop = 20;
            stoptime = 3f;

            dumbpops = false;// this deliberately pops at the wrong time.
            dumbpopradius = 220f;
            dumbinnerpopradius = 160f;
            dumbpopNRG = 80f;

            badlaser = false;
            badlaserAngle = 35f;

            multipleZap = 0;
            zapstartTime = 2.6f;
            zapendTime = 3.2f;
            dblZapstarttime = 3.6f;
            dblZapendtime = 4.2f;
            tplZapstarttime = 4.5f;
            tplZapendtime = 4.6f;

            pauseWhileLaser = true;

            MINtrack = 10f;
            MAXtrack = 30f;

            MINidle = 45;
            MAXidle = 120;
        }
        else if (thisAI == AItype.Bentham)
        {

            name.text = "Bentham";
            maxtrackdistance = 380f;
            mintrackdistance = 200f;
            trackspeed = 45f;

            possibleToloseInterest = false;
            chanceOfLosinginterest = 800;

            //periodicZap = false;
            //lasertimeron = .60f;
            //lasertimeoff = 3.2f;

            stopstracking = false;
            chanceoftotalstop = 20;
            stoptime = 3f;

            dumbpops = false;// this deliberately pops at the wrong time.
            dumbpopradius = 220f;
            dumbinnerpopradius = 160f;
            dumbpopNRG = 80f;

            badlaser = false;
            badlaserAngle = 35f;

            multipleZap = 0;
            zapstartTime = 2.6f;
            zapendTime = 3.2f;
            dblZapstarttime = 3.6f;
            dblZapendtime = 4.2f;
            tplZapstarttime = 4.5f;
            tplZapendtime = 4.6f;

            pauseWhileLaser = true;

            MINtrack = 10f;
            MAXtrack = 30f;

            MINidle = 45;
            MAXidle = 120;
        }
        else if (thisAI == AItype.Clayton)
        {

            name.text = "Clayton";
            maxtrackdistance = 380f;
            mintrackdistance = 200f;
            trackspeed = 45f;

            possibleToloseInterest = false;
            chanceOfLosinginterest = 800;

            //periodicZap = false;
            //lasertimeron = .60f;
            //lasertimeoff = 3.2f;

            stopstracking = false;
            chanceoftotalstop = 20;
            stoptime = 3f;

            dumbpops = false;// this deliberately pops at the wrong time.
            dumbpopradius = 220f;
            dumbinnerpopradius = 160f;
            dumbpopNRG = 80f;

            badlaser = false;
            badlaserAngle = 35f;

            multipleZap = 0;
            zapstartTime = 2.6f;
            zapendTime = 3.2f;
            dblZapstarttime = 3.6f;
            dblZapendtime = 4.2f;
            tplZapstarttime = 4.5f;
            tplZapendtime = 4.6f;

            pauseWhileLaser = true;

            MINtrack = 10f;
            MAXtrack = 30f;

            MINidle = 45;
            MAXidle = 120;
        }
        else if (thisAI == AItype.Addison)
        {

            name.text = "Addison";
            maxtrackdistance = 380f;
            mintrackdistance = 200f;
            trackspeed = 45f;

            possibleToloseInterest = false;
            chanceOfLosinginterest = 800;

            //periodicZap = false;
            //lasertimeron = .60f;
            //lasertimeoff = 3.2f;

            stopstracking = false;
            chanceoftotalstop = 20;
            stoptime = 3f;

            dumbpops = false;// this deliberately pops at the wrong time.
            dumbpopradius = 220f;
            dumbinnerpopradius = 160f;
            dumbpopNRG = 80f;

            badlaser = false;
            badlaserAngle = 35f;

            multipleZap = 0;
            zapstartTime = 2.6f;
            zapendTime = 3.2f;
            dblZapstarttime = 3.6f;
            dblZapendtime = 4.2f;
            tplZapstarttime = 4.5f;
            tplZapendtime = 4.6f;

            pauseWhileLaser = true;

            MINtrack = 10f;
            MAXtrack = 30f;

            MINidle = 45;
            MAXidle = 120;
        }

        currtrackdistance = mintrackdistance + (maxtrackdistance - mintrackdistance) * Random.Range(0.0f, 1.0f);
        
    }

    private void Update()
    {

        if (pauseWhileLaser && RSC.AIlaserON)
        {

            laserPause = true;
        }
        else
        {
            laserPause = false;
        }

        ball = (GameObject.FindGameObjectWithTag("ball")==null) ? Vector3.zero : GameObject.FindGameObjectWithTag("ball").transform.position;


        // up to triple periodic laser. no aiming
        if (zappsDone< multipleZap) {
            
            timer += Time.deltaTime;
            if (zappsDone == 0)
            {
                
                if ((timer > zapstartTime && !RSC.AIlaserON))
                {

                    RSC.AIlaserON = true;
                }
                if ((timer>zapendTime && RSC.AIlaserON)) {
                    RSC.AIlaserON = false;
                    zappsDone++;
                    if (zappsDone + 1 == multipleZap)
                    {
                        timer = 0;
                        zappsDone = 0;
                    }
                }
            }

            if (zappsDone == 1)
            {

                if ((timer > dblZapstarttime && !RSC.AIlaserON))
                {
                    RSC.AIlaserON = true;
                }
                if ((timer > dblZapendtime && RSC.AIlaserON))
                {
                    RSC.AIlaserON = false;
                    zappsDone++;
                    if (zappsDone + 1 == multipleZap)
                    {
                        timer = 0;
                        zappsDone = 0;
                    }
                }


            }

            if (zappsDone == 2)
            {
                if ((timer > tplZapstarttime && !RSC.AIlaserON))
                {
                    RSC.AIlaserON = true;
                }
                if ((timer > tplZapendtime && RSC.AIlaserON))
                {
                    RSC.AIlaserON = false;
                    zappsDone++;
                    if (zappsDone+1 == multipleZap) {
                        timer = 0;
                        zappsDone = 0;
                    }
                    
                }
            }



        }





        // laser that aims away from the player.
        if (badlaser) {
            if (Vector3.Angle(Vector3.right, -(ball - transform.position)) > badlaserAngle)
            {

                RSC.AIlaserON = true;
            }
            else {
                RSC.AIlaserON = false;
            }
        }


        





        if (dumbpops) {// poorly timed pops
            if (Vector3.Distance(ball, transform.position) < dumbpopradius && Vector3.Distance(ball, transform.position)>dumbinnerpopradius && RSC.Width > dumbpopNRG)
            {
                RSC.AIPop = true;
            }
            else {
                RSC.AIPop = false;
            }
        }

        //idle/tracking conditions
        if ( ball  == Vector3.zero || Mathf.Abs(ball.x - transform.position.x) > currtrackdistance) // idle behaviour conditions
        {
            if (!laserPause)    // so idling also stops if laser on
            {
                if (thisAI == AItype.Reuben || thisAI == AItype.Balthazar) {
                    VariableSpeedUpDown(MINidle, MAXidle);
                } else if (thisAI == AItype.Rupert || thisAI == AItype.Edwald) {
                    UpDownSQScaledSpeedTwoPoints(8, 15, 40, -40);
                }

           
                
                
                //simpleUpDown(120);
                //idleDance(120);
                //centrePointDance(120,10, 100);

                //ON CHANGE OF METHOD YOU WANNA SET THIS BOOL FALSE.
                //variableIdlespeedInitialised = false;
                //VariableSpeedUpDown(45, 120);
                //UpDownScaledSpeed(40,150, 50);
                //UpDownSQScaledSpeed(4, 15, 50);
                //UpDownScaledSpeedTwoPoints(30, 250, 170, -170);
                //UpDownSQScaledSpeedTwoPoints(8, 15, 40, -40);
            }

            // changes track distance when switches from tracking the ball. This may make tracking start...
            if (isBeingTracked) {
                isBeingTracked = false;
                lostInterest = false;
                Random.InitState((int)System.DateTime.Now.Ticks);
                currtrackdistance = mintrackdistance + (maxtrackdistance - mintrackdistance) * Random.Range(0.0f, 1.0f);
                //Debug.Log("not tracking - New track distance set: " + currtrackdistance);

                // randomises "speed" varaible used in auto tracking
                speed = randSpeed(MINtrack, MAXtrack);
            }


        }
        else {

            
            //bool for beingtracked
            if (!isBeingTracked) {
                isBeingTracked = true;
                //Debug.Log("TRacking begins at " + currtrackdistance);
            }


            // these two minimise track distance or pause tracking 

            // one in "chanceoflosinginterest" to minimise track distance.
            if (Random.Range(1,chanceOfLosinginterest) == 9 && !lostInterest && possibleToloseInterest) {
                currtrackdistance = mintrackdistance;
                lostInterest = true;
                Debug.Log("TrackDistance Minimised!!");
            }

            // chance of total stop for a few seconds
            if (stopstracking && Random.Range(0,chanceoftotalstop)==1 && !stopped) {
                stopped = true;
                Debug.Log("Tracking stopped for " + stoptime + " seconds");
            }



            if ( stopped && stoptimer< stoptime) {
                stoptimer += Time.deltaTime;
            }
            else if(!laserPause)
            {
                //trackball(trackspeed, 0.2f);

                //  IF CHANGING TO THIS METHOD SET variabletrackspeedinitialised TO FALSE.
                //variabletrackspeedinitialised =  false;
                Auto_trackball(MINtrack, MAXtrack, 0.2f);

                stopped = false;
                stoptimer = 0;
            }
        }
    }

    


    /// <summary>
    /// just up down
    /// </summary>
    /// <param name="idlespeed"></param>
    private void simpleUpDown(float idlespeed)
    {
        // limits
        if ((transform.position.y >= playerLimit && direction == +1) || (transform.position.y <= -playerLimit && direction == -1))
        {
            direction = -direction;
        }
        transform.position += new Vector3(0, idlespeed * Time.deltaTime * direction, 0);

    }

    /// <summary>
    /// up and down with a chance of changing direction
    /// </summary>
    /// <param name="idlespeed"></param>
    private void idleDance(float idlespeed) {

        // limits of idle wander
        if ((transform.position.y >= playerLimit && direction==+1) || (transform.position.y <= -playerLimit && direction==-1)) {
            direction = -direction;
        }

        if (Random.Range(0,40)== 1) {
            direction = -direction;
        }
        transform.position += new Vector3(0, idlespeed * Time.deltaTime * direction, 0);
    }

    /// <summary>
    /// up and down with a change in speed each time.
    /// </summary>
    /// <param name="min"></param>
    /// <param name="max"></param>
    private void VariableSpeedUpDown(float min, float max)
    {
        if (!variableIdlespeedInitialised)
        {
            variableTrackSpeed = min;
            variableIdlespeedInitialised = true;
        }

        // limits of idle wander
        if ((transform.position.y >= playerLimit && direction == +1) || (transform.position.y <= -playerLimit && direction == -1))
        {
            variableTrackSpeed = randSpeed(min, max);
            direction = -direction;
        }
        transform.position += new Vector3(0, variableTrackSpeed * Time.deltaTime * direction, 0);
    }

    /// <summary>
    /// moves from centrevalue till is turned back, or reaches limit. Constant speed.
    /// </summary>
    /// <param name="idlespeed"></param>
    /// <param name="resetdistance"></param>
    /// <param name="centreValue"></param>
    private void centrePointDance(float idlespeed, float resetdistance, float centreValue )
    {

        // resets bool if close enough to given centre 
        if ((transform.position.y < centreValue+ resetdistance || transform.position.y < centreValue-resetdistance) && backToCentre)
        {
            backToCentre = false;

        }


        // limits of idle wander
        if ((transform.position.y >= playerLimit && direction == +1) || (transform.position.y <= -playerLimit && direction == -1))
        {
            direction = -direction;
        }


        // if bool and further from centre
        if (Random.Range(0, 40) == 1 && direction==1 && transform.position.y > centreValue+resetdistance && !backToCentre)
        {
            backToCentre = true;
            direction = -direction;
        }


        if (Random.Range(0, 40) == 1 && direction == -1 && transform.position.y < centreValue-resetdistance && !backToCentre)
        {
            backToCentre = true;
            direction = -direction;
        }
        transform.position += new Vector3(0, idlespeed * Time.deltaTime * direction, 0);
    }

    /// <summary>
    /// simple up down scaled by distance from given point 
    /// </summary>
    /// <param name="idlespeed"></param>
    private void UpDownScaledSpeed(float idlespeedMin, float idlespeedMax, float centreVal)
    {
        float fraction = 1;

        // limits
        if ((transform.position.y >= playerLimit && direction == +1) || (transform.position.y <= -playerLimit && direction == -1))
        {
            direction = -direction;
        }

        if (transform.position.y >= centreVal)
        {
            fraction = (transform.position.y - centreVal) / (playerLimit - centreVal);
        }
        else {
            fraction = (transform.position.y - centreVal) / (-playerLimit- centreVal);
        }


        float speed = idlespeedMin + (idlespeedMax - idlespeedMin) * fraction;

        transform.position += new Vector3(0, speed * Time.deltaTime * direction, 0);

    }

    private void UpDownSQScaledSpeed(float idlespeedMin, float idlespeedMax, float centreVal)
    {
        float fraction = 1;

        // limits
        if ((transform.position.y >= playerLimit && direction == +1) || (transform.position.y <= -playerLimit && direction == -1))
        {
            direction = -direction;
        }

        if (transform.position.y >= centreVal)
        {
            fraction = (transform.position.y - centreVal) / (playerLimit - centreVal);
        }
        else
        {
            fraction = (transform.position.y - centreVal) / (-playerLimit - centreVal);
        }


        float speed = idlespeedMin + (idlespeedMax - idlespeedMin) * fraction;

        transform.position += new Vector3(0, speed * speed * Time.deltaTime * direction, 0);

    }




    private void UpDownScaledSpeedTwoPoints(float idlespeedMin, float idlespeedMax, float centreVal0, float centreVal1)
    {
        float fraction0 = 1;
        float fraction1 = 1;

        // limits
        if ((transform.position.y >= playerLimit && direction == +1) || (transform.position.y <= -playerLimit && direction == -1))
        {
            direction = -direction;
        }

        if (transform.position.y >= centreVal0)
        {
            fraction0 = (transform.position.y - centreVal0) / (playerLimit - centreVal0);
        }
        else
        {
            fraction0 = (transform.position.y - centreVal0) / (-playerLimit - centreVal0);
        }


        if (transform.position.y >= centreVal1)
        {
            fraction1 = (transform.position.y - centreVal1) / (playerLimit - centreVal1);
        }
        else
        {
            fraction1 = (transform.position.y - centreVal1) / (-playerLimit - centreVal1);
        }

        float frac = (fraction0 < fraction1) ? fraction0 : fraction1;


        float speed = idlespeedMin + (idlespeedMax - idlespeedMin) * frac;

        transform.position += new Vector3(0, speed * Time.deltaTime * direction, 0);

    }


    private void UpDownSQScaledSpeedTwoPoints(float idlespeedMin, float idlespeedMax, float centreVal0, float centreVal1)
    {
        float fraction0 = 1;
        float fraction1 = 1;

        // limits
        if ((transform.position.y >= playerLimit && direction == +1) || (transform.position.y <= -playerLimit && direction == -1))
        {
            direction = -direction;
        }

        if (transform.position.y >= centreVal0)
        {
            fraction0 = (transform.position.y - centreVal0) / (playerLimit - centreVal0);
        }
        else
        {
            fraction0 = (transform.position.y - centreVal0) / (-playerLimit - centreVal0);
        }


        if (transform.position.y >= centreVal1)
        {
            fraction1 = (transform.position.y - centreVal1) / (playerLimit - centreVal1);
        }
        else
        {
            fraction1 = (transform.position.y - centreVal1) / (-playerLimit - centreVal1);
        }

        float frac = (fraction0 < fraction1) ? fraction0 : fraction1;


        float speed = idlespeedMin + (idlespeedMax - idlespeedMin) * frac;

        transform.position += new Vector3(0, speed*speed * Time.deltaTime * direction, 0);

    }






    /// <summary>
    /// normal track ball at given speed
    /// </summary>
    /// <param name="speed"></param>
    /// <param name="precision"></param>
    private void Auto_trackball( float MINspeed, float MAXspeed, float precision)
    {
        float diff = ball.y - transform.position.y;
        float sign = Mathf.Sign(diff);


        if (!variabletrackspeedinitialised) {
            variabletrackspeedinitialised = true;
            speed = MINspeed;
            trackspeedSet = true;
        }

        if (diff * diff > precision)
        {
            float scaleFactor = Mathf.Abs(diff * diff / (Screen.height * scaletodistance));
            transform.position += new Vector3(0, speed * Time.deltaTime * sign * scaleFactor, 0);
            //Debug.Log("Track speed being used :  " + speed);
            trackspeedSet = false;
        }else if(!trackspeedSet)
        {
            speed = randSpeed(MINspeed, MAXspeed);

            trackspeedSet = true;
        }
    }

    private void trackball(float speed, float precision)
    {
        float diff = ball.y - transform.position.y;
        float sign = Mathf.Sign(diff);

        if (diff * diff > precision)
        {
            float scaleFactor = Mathf.Abs(diff * diff / (Screen.height * scaletodistance));
            transform.position += new Vector3(0, speed * Time.deltaTime * sign * scaleFactor, 0);
        }
    }




    private float randSpeed(float min,float max) {

        Random.InitState((int)System.DateTime.Now.Ticks);
        return min + (max - min) * Random.Range(0.0f, 0.999f);

    }

    


}
















//    private bool backtomiddlebooltop = true;
//    private bool backtomiddleboolbottom = true;

//    private int popdistance = 160;// Distance at which pops are done.


//    // speed at which ball is tracked
//    public float trackspeed00 = 300f;
//    public float trackspeed01 = 300f;
//    private float trackvariable;

//    public float idlespeed = 150f;

//    public bool limitedPop = false;
//    // distance at which tracking starts and random movement stops.
//    public float maxtrackdistance = 400;
//    public float mintrackdistance = Screen.width / 4.5f;

//    private float laserdistance = Screen.width / 10;

//    // bool for random movement of AI.
//    private bool up = false;

//    //shield variables
//    private GameObject Rshield;
//    private Right_Shield_Controller RSC;
//    private int popchoice = 0;


//    private RightLaser RL;
//    private GameObject ball;
//    private GameObject humanPlayer;

//    private float playerlimit = 185; // this is a magic number, for the max/min height.
//    private float idlerange = 70;


//    void Start()
//    {
        
//        Rshield = GameObject.FindGameObjectWithTag("Rshield");
//        RSC = Rshield.GetComponent<Right_Shield_Controller>();
//        humanPlayer = GameObject.FindGameObjectWithTag("L_Player_Container");

//        RL = GetComponent<RightLaser>();
//    }


//    void Update()
//    {
//        //Debug.Log("ball speed" + GameObject.FindGameObjectWithTag("ball").GetComponent<Rigidbody>().velocity);
        

//        //Player motion.
//        if (GameObject.FindGameObjectWithTag("ball") != null)
//        {
//            ball = GameObject.FindGameObjectWithTag("ball");// the AI only works for one ball, currently

//            if (Mathf.Abs(ball.transform.position.x - transform.position.x) < maxtrackdistance) // if CLOSER than maxtrackdist
//            {


//                trackvariable = trackspeed00 + Mathf.Abs(trackspeed00 - trackspeed01);  //  track at 2*trackspeed00 - trackspeed01?????
//                track(trackvariable);
                
//            }
//            else// or else idle WHEN GREATER THAN TRACKDIST
//            {

//                // these if statements make the AI idle in and around hte idlerange.
//                if (transform.position.y >= playerlimit || transform.position.y <= -playerlimit)// if hits limit turn up to !up
//                {
//                    up = !up;
//                }

//                if (transform.position.y >= idlerange && transform.position.y < playerlimit && backtomiddlebooltop && up )//so, in idlerange and 
//                {
                    
//                    if (Random.Range(0, 20) == 1)// 1/20 chance to reverse at any time.
//                    {
//                        up = !up;
//                        backtomiddlebooltop = false;
//                    }
//                }


//                if (transform.position.y <= -idlerange && transform.position.y > -playerlimit && backtomiddleboolbottom && !up)
//                {
//                    if (Random.Range(0, 20) == 1)
//                    {
//                        up = !up;
//                        backtomiddleboolbottom = false;
//                    }
//                }

//                if (transform.position.y >= -idlerange  && transform.position.y <= idlerange)
//                {
//                    backtomiddleboolbottom = true;
//                    backtomiddlebooltop = true;
//                    if (Random.Range(0, 100) == 1)
//                    {
//                        up = !up;
//                    }
//                }





//                if (up)
//                {
//                    transform.position += new Vector3(0, idlespeed * Time.deltaTime, 0);
//                }
//                else
//                {
//                    transform.position -= new Vector3(0, idlespeed * Time.deltaTime, 0);
//                }
//            }

//            // this fires the ai laser
//            //if ( Mathf.Abs(ball.transform.position.y- transform.position.y )<15)
//            //{


//            //    RSC.AIlaserON = true;

//            //}
//            //else
//            //{
//            //    RSC.AIlaserON = false;
//            //}

//            if ((Vector3.Distance(ball.transform.position, transform.position) < popdistance) &&
//                (Vector3.Distance(ball.transform.position, transform.position) >= popdistance - 10))
//            //this is a reasonable guess for the y distance from the player to the AI player.
//            {

//                Vector3 ballvec = ball.transform.position - transform.position;
//                Vector3 p1vec = humanPlayer.transform.position - transform.position;

//                if (Mathf.Abs(humanPlayer.transform.position.y - transform.position.y) > 80f && limitedPop == false)

//                    RSC.AIPop = true;

//                else if (Mathf.Abs(Vector3.Angle(ballvec, p1vec)) > 15)
//                {
//                    RSC.AIPop = true;
//                }
//                else
//                {
//                    RSC.AIPop = false;
//                }

//            }
//            else
//            {
//                RSC.AIPop = false;
//            }
//        }
//    }

//    private void track(float trackspeed)
//    {
//        if (ball.transform.position.y > transform.position.y)
//        {
//            transform.position += new Vector3(0, trackspeed * Time.deltaTime, 0);
//        }

//        if (ball.transform.position.y < transform.position.y)
//        {
//            transform.position += new Vector3(0, -trackspeed * Time.deltaTime, 0);
//        }
//    }

//    //IEnumerator resettrackchange(float pause)
//    //{
//    //    yield return new WaitForSeconds(pause);
//    //    changetrackdir = true;
//    //}
//}
