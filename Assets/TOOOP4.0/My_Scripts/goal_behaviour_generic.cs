﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// this midleading script is mainly dealing with mad mode which is not on the android version. This script needs deleting
/// </summary>
public class goal_behaviour_generic : MonoBehaviour {

    private float ballexplosionforce = 250f;

    private GameObject ballprefab;
    private Right_Shield_Controller RSC;//again assingning objects coz the objects get disabled.
    private Shield_Controller LSC;

    public AudioClip goal;
    private AudioSource source;

    public bool goalActive=true;// a bool to disable goal when there are more than one balls in play.
    

    void Awake()
    {
        //Debug.Log(GameObject.FindGameObjectsWithTag("ball") + "  " + GameObject.FindGameObjectWithTag("Lshield").GetComponent<Shield_Controller>().ballfireon
          //  + " " + GameObject.FindGameObjectWithTag("Rshield").GetComponent<Right_Shield_Controller>().ballfireon);
        //if (GameObject.FindGameObjectsWithTag("ball").Length==0 && GameObject.FindGameObjectWithTag("Lshield").GetComponent<Shield_Controller>().ballfireon == false
        //    && GameObject.FindGameObjectWithTag("Rshield").GetComponent<Right_Shield_Controller>().ballfireon == false)// should only spawn in non Mad modes...
        //{
        //    StartCoroutine(respawnBall(3.0f));
        //}
        source = GetComponent<AudioSource>();
        LSC = GameObject.FindGameObjectWithTag("Lshield").GetComponent<Shield_Controller>();
        RSC = GameObject.FindGameObjectWithTag("Rshield").GetComponent<Right_Shield_Controller>();

        //ball_init();
    }

    //private void OnTriggerEnter(Collider other)
    //{
    //    // assumes there are two goals only
    //    if (other.gameObject.tag == "ball" && GameObject.FindGameObjectsWithTag("goal")[0].GetComponent<goal_behaviour_generic>().goalActive
    //        && GameObject.FindGameObjectsWithTag("goal")[1].GetComponent<goal_behaviour_generic>().goalActive)//mad mode is paused if only one player has it.
    //    {
    //        if (RSC.ballfireon && LSC.ballfireon)// cease fire if both players are in mad mode.
    //        {
    //            StartCoroutine(ceaseFire());
    //        }


            
    //        else if (LSC.ballfireon || RSC.ballfireon)// the mad mode changes if only one player has it
    //        {
    //            LSC.ballfireon = !LSC.ballfireon;
    //            RSC.ballfireon = !RSC.ballfireon;
    //        }
    //        source.volume = 0.05f;
    //        source.PlayOneShot(goal);
    //        //other.gameObject.SetActive(false);
            
    //        StartCoroutine(blowupballs());
                
            
    //        //StartCoroutine(respawnBall(3.0f));
    //    }
    //}

   

    IEnumerator blowupballs()
    {
        yield return new WaitForSeconds(0.0001f);
        GameObject.FindGameObjectsWithTag("goal")[0].GetComponent<goal_behaviour_generic>().goalActive = false;     // assumes there are two goals only
        GameObject.FindGameObjectsWithTag("goal")[1].GetComponent<goal_behaviour_generic>().goalActive = false;


        GameObject[] obstacles = GameObject.FindGameObjectsWithTag("obstacle");
        GameObject[] balls = GameObject.FindGameObjectsWithTag("ball");



        for (int i = 0; i < balls.Length; i++)
        {
            

            yield return new WaitForSeconds(0.30f);
            Instantiate(Resources.Load("explosion"), balls[i].transform.position, Quaternion.identity);

            foreach (GameObject o in obstacles)
            {
                o.GetComponent<Rigidbody>().AddExplosionForce(ballexplosionforce, balls[i].transform.position, 200);
            }


            //GameObject[] remainingballs = GameObject.FindGameObjectsWithTag("ball");
            //foreach(GameObject b in remainingballs)
            //{
            //    b.GetComponent<Rigidbody>().AddExplosionForce(ballexplosionforce, balls[i].transform.position, 200);
            //}

            Destroy(balls[i]);
        }
        GameObject.FindGameObjectsWithTag("goal")[0].GetComponent<goal_behaviour_generic>().goalActive = true;     // assumes there are two goals only
        GameObject.FindGameObjectsWithTag("goal")[1].GetComponent<goal_behaviour_generic>().goalActive = true;
    }

    IEnumerator ceaseFire()// causes a brief ceasefire when the player is spawning.
    {
        LSC.ballfireon = false;
        RSC.ballfireon = false;
        yield return new WaitForSeconds(2.6f);
        LSC.ballfireon = true;
        RSC.ballfireon = true;
    }

}
