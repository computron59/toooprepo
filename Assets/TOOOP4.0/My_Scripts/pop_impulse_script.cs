﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pop_impulse_script : MonoBehaviour {

    
    private float popimpulse = 650f;
    

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "ball")
        {           
            other.gameObject.GetComponent<Rigidbody>().AddForce((other.transform.position-transform.position).normalized * popimpulse, ForceMode.Impulse);
            
        }
    }
}
