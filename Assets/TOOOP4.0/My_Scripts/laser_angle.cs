﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class laser_angle : MonoBehaviour {


    public float arc = 30f;
    public float rotSpeed = 10f;
    int dir = -1;
    Quaternion startRot;
    Vector3 initialdir;

    // Use this for initialization
    void Start () {
        
        initialdir = (transform.Find("aimer").position - transform.position).normalized;
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 currdir = (transform.Find("aimer").position - transform.position).normalized;
        float currentRot = Vector3.SignedAngle(initialdir, currdir, Vector3.forward);



        if (currentRot <= -arc/2 && dir == -1)
        {
            dir = 1;
        } else if (currentRot >= arc/2 && dir== 1) {
            dir = -1;
        }

        transform.Rotate(Vector3.forward* rotSpeed * Time.deltaTime* dir);

	}
}
