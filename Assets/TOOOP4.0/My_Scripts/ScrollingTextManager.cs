﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ScrollingTextManager : MonoBehaviour {

    //my method...
    //Text text;


    //public Text maintext;
    //Text clone;

    float initialpos;

    public TextMeshProUGUI maintext;
    TextMeshProUGUI clone;

    public float speed = 45;
    float width;
    RectTransform canvasRect;
    float canvasScaleX;
    float canvasScaleY;

    private void Start()
    {
        //GameObject aiparent = GameObject.FindGameObjectWithTag("aimask");
        initialpos = maintext.transform.position.x;
        maintext.transform.Rotate(0, 180, 0);
        //maintext.GetComponent<RectTransform>().localScale =
        //  new Vector3(maintext.preferredWidth* canvasScaleX, maintext.GetComponent<RectTransform>().localScale.y, maintext.GetComponent<RectTransform>().localScale.z);

        clone = GameObject.Instantiate(maintext) as TextMeshProUGUI;
        clone.transform.SetParent(maintext.transform.parent /*GameObject.FindObjectOfType<Canvas>().transform*/);

        //clone.text = maintext.text;
        //clone.fontSize = maintext.fontSize;


        canvasRect = GameObject.FindObjectOfType<Canvas>().GetComponent<RectTransform>();
        canvasScaleX = canvasRect.localScale.x;
        canvasScaleY = canvasRect.localScale.y;
        // scale the clone
        clone.GetComponent<RectTransform>().localScale =
            new Vector3(clone.GetComponent<RectTransform>().localScale.x * canvasScaleX, 
            clone.GetComponent<RectTransform>().localScale.y * canvasScaleY,
            clone.GetComponent<RectTransform>().localScale.z);
        //clone.transform.Rotate(0, 180, 0);


        width = maintext.preferredWidth*canvasScaleX;


            //maintext.GetComponent<RectTransform>().sizeDelta.x * maintext.GetComponent<RectTransform>().localScale.x;
        Debug.Log("Width is : " + width);
    }

    private void Update()
    {
        maintext.GetComponent<RectTransform>().transform.position += Vector3.right * speed * Time.deltaTime;
        clone.transform.position = maintext.GetComponent<RectTransform>().transform.position - Vector3.right * width;
        if (maintext.transform.position.x >initialpos+ width) {
            TextMeshProUGUI temp;
            temp = maintext;
            maintext = clone;
            clone = temp;
        }
    }




}
    ////old method
    //public TextMeshProUGUI TextMeshProComponent;
    //public float ScrollSpeed = 10f;

    //private TextMeshProUGUI m_cloneTextObject;

    //private RectTransform m_textRectTransform;
    //private string sourceText;
    //private string tempText;

    //bool hasTextChanged;

    //// Use this for initialization
    //private void Awake()
    //{
    //    m_textRectTransform = TextMeshProComponent.GetComponent<RectTransform>();

    //    m_cloneTextObject = Instantiate(TextMeshProComponent) as TextMeshProUGUI;
    //    RectTransform cloneRectTransform = m_cloneTextObject.GetComponent<RectTransform>();
    //    cloneRectTransform.transform.position = m_textRectTransform.transform.position - Vector3.right * TextMeshProComponent.bounds.size.x;

    //    //cloneRectTransform.sizeDelta /= 16;
    //    cloneRectTransform.SetParent(m_textRectTransform);
        
    //    //cloneRectTransform.anchorMin = new Vector2(1, 0.5f);
    //    //cloneRectTransform.anchorMax = new Vector2(1, 0.5f);
    //    //cloneRectTransform.localPosition = Vector2.zero;
    //    //cloneRectTransform.localScale = new Vector3(1, 1, 1);
    //}



    //private void OnEnable()
    //{
    //    TMPro_EventManager.TEXT_CHANGED_EVENT.Add(ON_TEXT_CHANGED);
    //}

    //private void OnDisable()
    //{
    //    TMPro_EventManager.TEXT_CHANGED_EVENT.Remove(ON_TEXT_CHANGED);
    //}

    ////checks to see if it the text has changed 
    //void ON_TEXT_CHANGED(Object obj)
    //{
    //    if (obj == TextMeshProComponent) hasTextChanged = true;
    //}


    //// Update is called once per frame
    //IEnumerator Start()
    //{

    //    float width = TextMeshProComponent.bounds.size.x;
    //    Vector3 startPosition = m_textRectTransform.position;

    //    float scrollPosition = 0;

    //    while (true)
    //    {
    //        if (hasTextChanged)
    //        {
    //            width = TextMeshProComponent.preferredWidth;
    //            m_cloneTextObject.text = TextMeshProComponent.text;
    //        }

    //        if (m_cloneTextObject.rectTransform.position.x <= width)
    //        {

    //            scrollPosition = -m_cloneTextObject.rectTransform.position.x;
    //        }

    //        Debug.Log(scrollPosition);
    //        m_textRectTransform.position = new Vector3((scrollPosition % width), startPosition.y, startPosition.z);

    //        scrollPosition += ScrollSpeed * 20 * Time.deltaTime;

    //        yield return null;
    //    }






    //}


    // newer method
    //public TextMeshProUGUI TextMeshProComponent;
    //public float ScrollSpeed = 10f;

    //private TextMeshProUGUI m_cloneTextObj;

    //private RectTransform m_textRectTransform;
    //private string origText;
    //private bool hasTextChanged;

    //// Use this for initialization
    //private void Awake()
    //{
    //    m_textRectTransform = TextMeshProComponent.GetComponent<RectTransform>();

    //    m_cloneTextObj = Instantiate(TextMeshProComponent) as TextMeshProUGUI;
    //    RectTransform cloneRectTransform =
    //        m_cloneTextObj.GetComponent<RectTransform>();
    //    cloneRectTransform.SetParent(m_textRectTransform);
    //    cloneRectTransform.anchorMin = new Vector2(1, 0.5f);
    //    cloneRectTransform.localScale = new Vector3(1, 1, 1);
    //}

    //private void OnEnable()
    //{
    //    TMPro_EventManager.TEXT_CHANGED_EVENT.Add(ON_TEXT_CHANGED);
    //}

    //private void OnDisable()
    //{
    //    TMPro_EventManager.TEXT_CHANGED_EVENT.Remove(ON_TEXT_CHANGED);
    //}

    ////checks to see if it the text has changed 
    //void ON_TEXT_CHANGED(Object obj)
    //{
    //    if (obj == TextMeshProComponent) hasTextChanged = true;
    //}


    //// Update is called once per frame
    //IEnumerator Start()
    //{

    //    float width = TextMeshProComponent.preferredWidth;
    //    Vector3 startPosition = m_textRectTransform.position;

    //    float scrollPosition = 0;

    //    while (true)
    //    {
    //        if (hasTextChanged)
    //        {
    //            width = TextMeshProComponent.preferredWidth;
    //            m_cloneTextObj.text = TextMeshProComponent.text;
    //        }

    //        m_cloneTextObj.rectTransform.position = new Vector3(m_cloneTextObj.rectTransform.position.x, -4.205f, m_cloneTextObj.rectTransform.position.z);

    //        //if (m_cloneTextObj.rectTransform.position.x <= 15)
    //        //{

    //        //    scrollPosition = -m_cloneTextObj.rectTransform.position.x;
    //        //}

    //        m_textRectTransform.position = new Vector3((-scrollPosition % width), startPosition.y, startPosition.z);
    //        scrollPosition += ScrollSpeed * Time.deltaTime;
    //        yield return null;
    //    }






    //}
//}
