﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cubeRotate : MonoBehaviour {


    public float Xrot = 15;
    public float Yrot = 15;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        transform.Rotate(Xrot*Time.deltaTime, Yrot * Time.deltaTime,0);
	}
}
