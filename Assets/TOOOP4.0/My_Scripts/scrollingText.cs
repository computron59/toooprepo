﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scrollingText : MonoBehaviour {

    public string message = "Where we're going, we don't need roads.";
    public float scrollSpeed = 50;

    Rect messageRect;

    float w,h,x,y;

    private void Start()
    {
        w = this.GetComponent<RectTransform>().sizeDelta.x;
        h = this.GetComponent<RectTransform>().sizeDelta.y;
        messageRect = new Rect(transform.position.x, transform.position.y, w, h);
    }

    void OnGUI()
    {
        // Set up message's rect if we haven't already.
        if (messageRect.width == 0)
        {
            var dimensions = GUI.skin.label.CalcSize(new GUIContent(message));

            // Start message past the left side of the screen.
            messageRect.x = -dimensions.x;
            messageRect.width = dimensions.x;
            messageRect.height = dimensions.y;
        }

        messageRect.x += Time.deltaTime * scrollSpeed;

        // If message has moved past the right side, move it back to the left.
        if (messageRect.x > Screen.width)
        {
            messageRect.x = -messageRect.width;
        }

        GUI.Label(messageRect, message);
    }
}
