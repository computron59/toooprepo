﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sonic_generic_script : MonoBehaviour {

    private float sonicimpulse = 100f;
    private float megasonicimpulse = 600f;
    private bool onesfxbool = true;

    private float sonicexplosiveforce = 100f;
    private float megasonicexplosiveforce = 300f;
    private float sonicradius = 50f;


    private AudioClip sonicspawn;
    private AudioClip sonicpop;

    private AudioSource source;

    public bool popsonic = false;

    Animator animator;

    // Use this for initialization
    void Start()
    {
        sonicspawn = Resources.Load("audio/laserKiss", typeof(AudioClip)) as AudioClip;
        sonicpop = Resources.Load("audio/StunGun00", typeof(AudioClip)) as AudioClip;
        source = this.GetComponent<AudioSource>();
        animator = GetComponent<Animator>();
        source.PlayOneShot(sonicspawn, 0.10f);
    }


    // Update is called once per frame
    void Update () {

 //       if (popsonic && onesfxbool)
 //       {
 //           onesfxbool = false;
 //           source.PlayOneShot(sonicpop, 0.045f);
 //       }

 //       animator.SetBool("popthesonic", popsonic);

	//}

 //   private void OnTriggerEnter(Collider other)
 //   {
 //       if (other.tag== "goal" || other.tag == "Rshield" || other.tag == "Lshield" || other.tag == "R_Player_PopShield" || other.tag == "L_Player_PopShield")
 //       {
            
 //           popsonic = true;
 //       }
 //       else if ((this.tag == "Lsonic00" || this.tag == "Rsonic00" || this.tag == "LMegasonic" || this.tag == "RMegasonic") &&
 //          (other.tag == "Lsonic00" || other.tag == "Rsonic00" || other.tag == "LMegasonic" || other.tag == "RMegasonic"))
 //       {
           
 //           popsonic = true;
 //       }else if (this.tag == "Lsonic00" && other.tag == "ball")
 //       {
 //           other.GetComponent<Rigidbody>().AddForce(Vector3.right*sonicimpulse, ForceMode.Impulse);
            
 //           popsonic = true;
 //       }
 //       else if (this.tag == "LMegasonic" && other.tag == "ball")
 //       {
 //           other.GetComponent<Rigidbody>().AddForce(Vector3.right * megasonicimpulse, ForceMode.Impulse);
            
 //           popsonic = true;
 //       }
 //       else if (this.tag == "Rsonic00" && other.tag == "ball")
 //       {
 //           other.GetComponent<Rigidbody>().AddForce(Vector3.left * sonicimpulse, ForceMode.Impulse);
           
 //           popsonic = true;
 //       }
 //       else if (this.tag == "RMegasonic" && other.tag == "ball")
 //       {
 //           other.GetComponent<Rigidbody>().AddForce(Vector3.left * megasonicimpulse, ForceMode.Impulse);
           
 //           popsonic = true;
 //       }else if(this.tag == "Lsonic00" || this.tag == "Rsonic00" && other.tag == "obstacle")// yet to be implemented obstacles
 //       {
 //           other.GetComponent<Rigidbody>().AddExplosionForce(sonicexplosiveforce, this.transform.position, sonicradius,0.0f, ForceMode.Impulse);
          
 //           popsonic = true;
 //       }
 //       else if (this.tag == "LMegasonic" || this.tag == "RMegasonic" && other.tag == "obstacle")// yet to be implemented obstacles
 //       {
 //           other.GetComponent<Rigidbody>().AddExplosionForce(megasonicexplosiveforce, this.transform.position, sonicradius, 0.0f, ForceMode.Impulse);
            
 //           popsonic = true;
 //       }
    }

    //void destroyme()
    //{
    //    source.PlayOneShot(sonicpop, 0.045f);
    //    Destroy(this.gameObject);
    //}
}
