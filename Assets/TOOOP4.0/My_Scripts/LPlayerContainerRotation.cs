﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LPlayerContainerRotation : MonoBehaviour {
    public bool clockwise = true;
    public float rotationSpeed = 15.0f;

    // fixedupdate should give a consistant rotation
    void FixedUpdate()
    {

        if (clockwise)
        {
            transform.Rotate(new Vector3(-rotationSpeed, 0, 0));
        }
        else
        {
            transform.Rotate(new Vector3(rotationSpeed, 0, 0));
        }
    }
}
