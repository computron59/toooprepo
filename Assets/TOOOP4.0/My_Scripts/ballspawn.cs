﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ballspawn : MonoBehaviour {

    bool spawnonce = true;
    public enum spawnTypes { randomVerticalCentre, belowFlap, avoidCentrePin }
    public spawnTypes thisLevelSpawn;

    //here i am assigning objects instead of finding by tag as these objects are not destroyed/spawned
    // they are dis/en abled
    private GameObject LShield;
    private GameObject RShield;


    // Use this for initialization
    void Start () {
        LShield = GameObject.FindGameObjectWithTag("Lshield");
        RShield = GameObject.FindGameObjectWithTag("Rshield");

    }
	
	// Update is called once per frame
	void Update () {
        if (GameObject.FindGameObjectsWithTag("ball").Length == 0
            && LShield.GetComponent<Shield_Controller>().ballfireon == false
            && RShield.GetComponent<Right_Shield_Controller>().ballfireon == false
            && spawnonce
            && LShield.GetComponent<Shield_Controller>().madmode == false
            && RShield.GetComponent<Right_Shield_Controller>().madmode == false)// should only spawn in non Mad modes...
        {
            spawnonce = false;
            StartCoroutine(respawnBall(3.0f));
        }
    }

    IEnumerator respawnBall(float t)
    {
        yield return new WaitForSeconds(t);
        // ball init script

        //switch (thisLevelSpawn) {
        //    case spawnTypes.randomVerticalCentre:
        //        ball_init_vertCentre();
        //        break;
        //    case spawnTypes.belowFlap:
        //        ball

        //}
        ball_init_vertCentre();

    }

    private void ball_init_vertCentre()
    {
        //ball.gameObject.SetActive(true); changed this to create/destroy balls

        Random.InitState((int)System.DateTime.Now.Ticks);
        double yoffset = Random.Range(-100, 100);
        GameObject ballspawned = Instantiate(Resources.Load("Ball_Container"), new Vector3(0, (float)yoffset, 0), Quaternion.identity) as GameObject;
        //ballprefab.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
        if (GameObject.FindGameObjectWithTag("congfigObj").GetComponent<level_Config>().gravity )
        {
            ballspawned.GetComponent<ball_Gravity_Script>().gravity_On = true;
        }
        spawnonce = true;
    }
}
