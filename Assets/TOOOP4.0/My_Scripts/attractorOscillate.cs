﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class attractorOscillate : MonoBehaviour {

    private Vector3 startPos;
    Vector3 dir;
    public float oscillateSpeed;
    public float oscillateDist;
    int direction = 1;

    bool signchanged = false;

	// Use this for initialization
	void Start () {
        startPos = transform.position;
        dir = (transform.Find("aimer").position - startPos).normalized;
	}
	
	// Update is called once per frame
	void Update () {
        //float side = transform.InverseTransformPoint(startPos);

        float dist = Vector3.Distance(transform.position, startPos);


        if (dist >= oscillateDist && !signchanged) {
            signchanged = true;
            direction = -direction;
        }

        if (dist<= 1.0f && signchanged) {
            signchanged = false;
        }

        transform.Translate(dir*Time.deltaTime* oscillateSpeed* direction);
	}
}
