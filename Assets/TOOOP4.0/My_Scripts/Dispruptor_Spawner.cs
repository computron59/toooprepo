﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dispruptor_Spawner : MonoBehaviour {

    public float spawnVel = 50;
    public float maxSpawnPause = 0.2f;

    float timer = 0;

    private GameObject disruptor;
    private Transform aimer, horiz;
    //float xmin, xmax;
    //Mesh cubeMesh;
    Collider cubeCollider;
    //Bounds b;


	// Use this for initialization
	void Start () {
        disruptor = Resources.Load("DisruptorParticle") as GameObject;
        aimer = transform.Find("aimer");
        horiz = transform.Find("horizontal");
        //b = mesh.bounds;
        //cubeMesh = GetComponent<MeshFilter>().mesh;
        cubeCollider = GetComponent<Collider>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        Vector3 dir = (aimer.position - transform.position).normalized;
        Vector3 perpdir = (horiz.position- transform.position).normalized;

        Vector3 X0 = transform.position - cubeCollider.bounds.size.x * 0.5f * perpdir + dir * 15;
        Vector3 X1 = transform.position + cubeCollider.bounds.size.x * 0.5f * perpdir + dir *15;

        Vector3 across = X1 - X0;

        Vector3 randPos = X0 + across * Random.Range(0.0f,1.0f);

        timer += Time.deltaTime;

        if (timer >= maxSpawnPause) {
            timer = 0;
            GameObject lastDispruptor = Instantiate(disruptor, randPos, Quaternion.identity);
            lastDispruptor.GetComponent<Rigidbody>().velocity = dir * spawnVel;
        }


    }
}
