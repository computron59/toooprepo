﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Right_Player_Movement_Script : MonoBehaviour {

    public bool enforcePCmovement = false;

    float width = Screen.height * 0.45f;
    bool istouch = false;
    touchReceiver touchInpt;
    public float scaletodistance = 2.3f;


    private void Start()
    {
        touchInpt = GameObject.FindObjectOfType<touchReceiver>();
        if (touchInpt.istouch == true) { istouch = true; }
    }

    // this is public and can be tweaked
    [SerializeField]
    public float movespeed = 800;

    // Update is called once per frame
    void FixedUpdate() {


        if (!istouch || enforcePCmovement) {
            float p = Input.GetAxis("VerticalP2");
            float width = 185;// this is a magic number for the max/min height

            // the barriers are here set
            if ((p > 0.2 && transform.position.y <= width) || (p < -0.2 && transform.position.y > -width))
            {

                // I am doing this this way because i don't want to introduce forces to what is already 
                // a well defined play system.
                transform.Translate(new Vector3(0, movespeed * p * Time.deltaTime, 0));
            }
        }
        else
        {
            //transform.position = new Vector3(transform.position.x , 0, transform.position.z);
            float y = touchInpt.RPlayerY;
            if ((transform.position.y <= width) || (transform.position.y > -width)/* && Mathf.Abs((y - Screen.height / 2) - transform.position.y) > 30*/)
            {
                float scalespeed = Mathf.Abs((y - transform.position.y)* (y - transform.position.y) / (Screen.height * scaletodistance));
                //float dir = Mathf.Sign(y - Screen.height / 2 - transform.position.y);
                //transform.Translate(new Vector3(0, movespeed * 0.005f * dir, 0));
                // if below increnmet
                if ((transform.position.y) < (y + 0.0005))
                {
                    transform.Translate(new Vector3(0, movespeed * Time.deltaTime* scalespeed, 0));
                }


                // if above devrement.
                if ((transform.position.y) > (y - 0.0005))
                {
                    transform.Translate(new Vector3(0, -movespeed * Time.deltaTime * scalespeed, 0));
                }
            }
            //else if ((transform.position.y <= width) || (transform.position.y > -width) && (y - transform.position.y) > 0.2)
            //{
            //    transform.Translate(0, movespeed * 0.05f * Time.deltaTime, 0);

            //}
        }
    }
}
