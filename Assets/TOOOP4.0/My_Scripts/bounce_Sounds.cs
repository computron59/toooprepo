﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bounce_Sounds : MonoBehaviour {

    private AudioSource source;
    public AudioClip bounce0;
    public AudioClip bounce1;

    // Use this for initialization
    void Start () {
        source = GetComponent<AudioSource>();
	}

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Rshield" || collision.gameObject.tag == "Lshield")
        {
            source.PlayOneShot(bounce0);
        }

        if (collision.gameObject.tag == "wall")
        {
            source.PlayOneShot(bounce1);
        }

    }
}
